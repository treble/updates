<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUpdatesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('updates', function(Blueprint $table)
		{
                        $table->string('id');
                        $table->primary('id');
                        $table->integer('user_id');
                        $table->string('username');
                        $table->integer('network_id');
                        $table->string('network_name');
                        $table->string('update_type');

                        $table->string('profile_image');    
                        
                        $table->string('title1');
                        $table->string('title2');
                        
                        $table->string('name1');
                        $table->string('name2');
                        $table->string('name3');
                        $table->string('name4');
                        
                        $table->string('text1');
                        $table->string('text2');
                        $table->string('text3');
                        $table->string('text4');
                        
                        $table->string('media1');
                        $table->string('media2');
                        $table->string('media3');
                        $table->string('media4');
                        
                        $table->string('source_url');
                        
                        $table->string('outside_url1');
                        $table->string('outside_url2');
                        $table->string('outside_url3');
                        $table->string('outside_url4');
                       
                        $table->integer('like_count');
                        $table->integer('comment_count');
                        $table->integer('share_count');
                        
                        $table->boolean('bookmarked')->default('0');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_networks');
	}

}
