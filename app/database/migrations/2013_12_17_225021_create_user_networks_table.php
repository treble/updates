<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserNetworksTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('user_networks', function(Blueprint $table)
		{
                        $table->integer('user_id');
                        $table->integer('network_id');
                        $table->primary(array('user_id', 'network_id'));
                        $table->string('username');
                        $table->string('network_name');
                        $table->string('access_token', '512');
                        $table->string('access_token2', '512');
                        $table->boolean('locked')->default('1');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_networks');
	}

}
