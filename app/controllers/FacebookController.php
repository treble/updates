<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class FacebookController extends BaseController {
    
    /** @var int network id */
    const FACEBOOK_NETWORK_ID   = 1;    
    /** @var string $app_id */
    const FACEBOOK_APP_ID       = "554149144671102";
    /** @var string $app_secret */
    const FACEBOOK_APP_SECRET	= "dddad2f4503df60e7c4613f8fafb7ccb";
        
    public function authorizeFacebook() {

        /* facebook OAuth object */
        $facebook = new Facebook(array(
            'appId'     => self::FACEBOOK_APP_ID,
            'secret'	=> self::FACEBOOK_APP_SECRET,
        ));

        /** @var int $user */
        $user = $facebook->getUser();

        if($user == 0){
            /* Authorize with facebook */
            //facebook callback is registered in the facebook application
            $loginUrl = $facebook->getLoginUrl(array("scope"=>"user_about_me, read_stream","display"=>"page"));

            return Redirect::to($loginUrl);
        } 
        
        /** @var string $access_token */
        $access_token = $facebook->getAccessToken();
        
        if (!empty($access_token) && (isset($user) && $user != 0)) {
            
            $uid = Auth::user()->id;
            $nid = self::FACEBOOK_NETWORK_ID;

            $userNetwork = UserNetwork::getRecord($uid, $nid);

            if (count($userNetwork) == 1) {

                UserNetwork::updateAccessTokens($uid, $nid, $access_token, $user);

            } else {
                $userNetwork = new UserNetwork();

                $userNetwork->user_id = Auth::user()->id;
                $userNetwork->username = User::find(Auth::user()->id)->username;
                $userNetwork->network_id = self::FACEBOOK_NETWORK_ID;
                $userNetwork->network_name = Network::find($userNetwork->network_id)->network_name;
                $userNetwork->locked = '0';
                $userNetwork->access_token = $access_token;
                $userNetwork->access_token2 = $user;

                $userNetwork->save();
            }
        } 

        return Redirect::to('authorize');
                
    }
    
    /**
     *Returns a json string of updates related to the account credentials given.
     * */
    public static function retrieveFacebook($limit = 10){
        $uid = Auth::user()->id;
        $nid = self::FACEBOOK_NETWORK_ID;   
        
        $record = UserNetwork::getRecord($uid, $nid);
        
        if (count($record) !== 0) {
            $access_token = $record[0]->access_token;
            
            $last_retrieve = $record[0]->last_retrieve;

            /* facebook OAuth object */
            $facebook = new Facebook(array(
                'appId'     => self::FACEBOOK_APP_ID,
                'secret'    => self::FACEBOOK_APP_SECRET,
            ));
            
            /** @var int $user */
            $user = $facebook->getUser();   
            if($user == 0){
                /* Authorize with facebook */
                //facebook callback is registered in the facebook application
                $loginUrl = $facebook->getLoginUrl(array("scope"=>"user_about_me, read_stream"));

                return Redirect::to($loginUrl);
            }
            $facebook->setExtendedAccessToken();
            $access_token = $facebook->getAccessToken();
            
            $yesterday = strtotime('yesterday');
            /* Get the user facebook home news feed */
//            $feed = $facebook->api('/'.$user.'/home?access_token='. $access_token . '&limit=50');
            $feed = $facebook->api('/'.$user.'/home?access_token='. $access_token . '&since='.$yesterday.'&limit='.$limit);
            
//            include_once('debug.php');
            if (count($feed['data']) != 0) {
                foreach ($feed['data'] as $data) { 

                    // Check the title to see if its worth continuing
                    $title = NULL;
                    if (isset($data['story'])) {
                        $title = $data['story'];
                    } else {
                        $title = $data['from']['name'] . ' posted:';
                    }                
//                    if ( stristr($title, $data['from']['name'].' likes ') || 
//                           stristr($title, $data['from']['name'].' commented on ') ) {
                    if ( stristr($title, ' likes ') ||                     
                       stristr($title, ' commented on ') ) {
                       continue; //skip 
                    }

                    $updateId = Auth::user()->id.'_'.self::FACEBOOK_NETWORK_ID.'_'.$data['id'];

                    $likes = $facebook->api('/'.$data['id'].'/likes?summary=true&limit=1&access_token='. $access_token);

                    $likes_count = isset($likes['summary']['total_count'])?$likes['summary']['total_count']:NULL;

                    $comments = $facebook->api('/'.$data['id'].'/comments?summary=true&limit=1&access_token='. $access_token);

                    $comments_count = isset($comments['summary']['total_count'])?$comments['summary']['total_count']:NULL;

                    $fb_user_id = $data['from']['id'];

                    $picture = $facebook->api('/'.$fb_user_id.'/picture?type=square&height=100&weight=100&redirect=0');

                    $pictureURL = $picture['data']['url'];

                    if (count(Update::withTrashed()->find($updateId)) === 0) {         

                        $update = new Update();

                        $update->id = $updateId; // The unique ID returned by the network with the network ID attached as prefix (ie 1_1243712894219348712). use the constant

                        $update->event_datetime = gmdate("Y-m-d H:i:s", strtotime($data['created_time'])); // timestamp of when the event happened

                        $update->user_id = Auth::user()->id; // the logged in user. dont change this unless you know what youre doing
                        $update->username = Auth::user()->username; // this too

                        $update->network_id = self::FACEBOOK_NETWORK_ID; // use the constant provided in each integration (I will probably move those constants to a global somewhere) 
                        $update->network_name = 'facebook'; // name of the network.

                        $update->update_type = $data['type'] . (isset($data['status_type'])?' - '.$data['status_type']:'') ; // type of update. dependent on each network if network has multiple types.

                        $update->profile_image = $pictureURL; // url to the profile image. 

                        $update->title1 = $title;  
                        $update->title2 = NULL; // use full name or username?

                        $update->name1 = $data['from']['name']; // not sure.. names i guess. save names here for whatever. or dont, im just a comment not a cop.
                        $update->name2 = NULL; 
                        $update->name3 = NULL; 
                        $update->name4 = NULL; 

                        $text = array();
                        if (isset($data['message'])&&!empty($data['message'])) {
                            $text[] = $data['message'];
                        } 
                        if (isset($data['name'])&&!empty($data['name'])) {
                            $text[] = $data['name'];
                        } 
                        if (isset($data['description'])&&!empty($data['description'])) {
                            $text[] = $data['description'];
                        }

                        $update->text1 = isset($text[0])?$text[0]:NULL; 
                        $update->text2 = isset($text[1])?$text[1]:NULL; 
                        $update->text3 = isset($text[2])?$text[2]:NULL; 
                        $update->text4 = NULL; 

                        $update->media_small_1 = isset($data['picture'])?$data['picture']:NULL; 
                        $update->media_small_2 = NULL;
                        $update->media_small_3 = NULL;
                        $update->media_small_4 = NULL; 

                        $image = NULL;
                        
                        if (isset($data['picture'])) {
                            $url = str_replace('_s.jpg', '_n.jpg', $data['picture']);
                        } else {
                            $url = NULL;
                        }
                        
                        if (!empty($url)) {
                            /* CHECK IF IMAGE IS VALID */
                            $handle = curl_init($url);
                            curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);

                           /* Get the HTML or whatever is linked in $url. */
                            $response = curl_exec($handle);
                            if ($response) {
                                /* Check for 404 (file not found). */
                                $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
                                if($httpCode == 404 || $httpCode == 403) {
                                    $image = $data['picture'];
                                } else {
                                    $image = $url;
                                }                                
                            }
                         
                            curl_close($handle);                            
                        }


                        $update->media_large_1 = $image;
                        $update->media_large_2 = NULL;
                        $update->media_large_3 = NULL;
                        $update->media_large_4 = NULL;

                        $post_source = explode('_', $data['id'], 2);

                        $update->source_url = 'https://www.facebook.com/'.$post_source[0].'/posts/'.$post_source[1]; // for the "view on blah*"  * = network name | cant be null

                        $update->outside_url1 = isset($data['link'])?$data['link']:NULL; // outside linking url for whatever (news shards, linked stuff, other stuff.. iono)
                        $update->outside_url2 = NULL; 
                        $update->outside_url3 = NULL; 
                        $update->outside_url4 = NULL; 

                        $update->statistic1 = $likes_count; // the count of likes
                        $update->statistic2 = $comments_count; // the count of comments
                        $update->statistic3 = isset($data['shares'])?$data['shares']['count']:0; 
                        $update->statistic4 = NULL; 

                        $update->bookmarked = '0'; // defaults to 0. delete this line even... unless you wanna auto bookmark something

                        $update->save();
                        // DEBUG
                        //echo 'saved this shit<br>';
                    } else {

                        $update = Update::withTrashed()->find($updateId);
                        
                        $update->profile_image = $pictureURL; // url to the profile image. 

                        // if it already exists, just update the statistics
                        $update->statistic1 = $likes_count; // the count of likes
                        $update->statistic2 = $comments_count; // the count of comments
                        $update->statistic3 = isset($data['shares'])?$data['shares']['count']:0; 
                        $update->statistic4 = NULL;

                        $update->save();

        //                echo 'duplicate. statistics updated.<br>'; //DEBUG
                    }
//                     echo $update;//DEBUG
//                     echo "<br><br><br>**********************<br><br><br>";//DEBUG
                 }
            }
             
            $time = strtotime('10 minutes ago');
            UserNetwork::updateLastRetrieve($uid, $nid, $time);
            
        }
        

    }
    
    public function deauthorizeFacebook(){
                
        $record = UserNetwork::where('user_id', '=', Auth::user()->id)
                    ->where('network_id', '=', self::FACEBOOK_NETWORK_ID)
                    ->take(1);

        $record->delete();
        
        $updates = Update::where('user_id', '=', Auth::user()->id)
                ->where('network_id', '=', self::FACEBOOK_NETWORK_ID)
                ->get();
        
        foreach ($updates as $update) {
            $update->forceDelete();
        }
        
        return Redirect::to('authorize');

    }    

}