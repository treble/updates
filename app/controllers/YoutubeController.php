<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
        include_once('debug.php');

class YoutubeController extends BaseController {
    
    /** @var int network id */
    const YOUTUBE_NETWORK_ID   = 4;    
    /** @var string $app_id */
    const YOUTUBE_CLIENT_ID     = "756879951597-5qn5b9fb0q2qrc4v84cpf0khbgp5aquf.apps.googleusercontent.com";
    /** @var string $app_secret */
    const YOUTUBE_CLIENT_SECRET	= "k7ZhuUlM_PZCnCbjQbVpo2Ij";
    
    const YOUTUBE_SERVER_API_KEY = 'AIzaSyCScQPhteKCj9V9LO8kTFsxvxzMxH-zNeY';

    
    
    public function authorizeYoutube() {
        /** @var string $url url for retrieving oauth code */
        $url = "https://accounts.google.com/o/oauth2/auth";

        /** @var array $params parameters to retrieve oauth code */
        $params = array(
            "response_type" => "code",  
            "client_id" => self::YOUTUBE_CLIENT_ID,
            "redirect_uri" => Config::get('app.base_url')."/authorize/youtube/redirect",
            "scope" => "https://www.googleapis.com/auth/youtube",
            "access_type" => "offline",
            "approval_prompt" => "force"
        );

        $request_to = $url . '?' . http_build_query($params);
        return Redirect::to($request_to);

    }
    
    public static function verifyYoutube() {
        
        if(isset($_GET['code'])) {
            /** @var string $code oauth code */
            $code = $_GET['code'];
            
            /** @var string $url url to retrieve access token */
            $url = 'https://accounts.google.com/o/oauth2/token';
            $params = array(
                "code" => $code,
                "client_id" => self::YOUTUBE_CLIENT_ID,
                "client_secret" => self::YOUTUBE_CLIENT_SECRET,
                "redirect_uri" => Config::get('app.base_url')."/authorize/youtube/redirect",
                "grant_type" => "authorization_code"
            );

            /* Compile curl call and retrieve access token */
            $curl = curl_init($url);    // we init curl by passing the url
            curl_setopt($curl,CURLOPT_POST,true);   // to send a POST request
            curl_setopt($curl,CURLOPT_POSTFIELDS,$params);   // indicate the data to send
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   // to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   // to stop cURL from verifying the peer's certificate.
            $result = curl_exec($curl);   // to perform the curl session
            curl_close($curl);   // to close the curl session

            $arr = json_decode($result,true);
            
            if (isset($arr['access_token'])) {
                $access_token = $arr['access_token'];
                if (isset($arr['refresh_token'])) {
                    $refresh_token = $arr['refresh_token'];
                }
            } else {
                $access_token = NULL;
                $refresh_token = NULL;
            }
            
            if (!empty($access_token)) {
                
                $uid = Auth::user()->id;
                $nid = self::YOUTUBE_NETWORK_ID;
                
                $userNetwork = UserNetwork::getRecord($uid, $nid);
           
                if (count($userNetwork) == 1) {
                    
                    UserNetwork::updateAccessTokens($uid, $nid, $access_token, $refresh_token);
                    
                } else {
                    $userNetwork = new UserNetwork();
                
                    $userNetwork->user_id = Auth::user()->id;
                    $userNetwork->username = User::find(Auth::user()->id)->username;
                    $userNetwork->network_id = self::YOUTUBE_NETWORK_ID;
                    $userNetwork->network_name = Network::find($userNetwork->network_id)->network_name;
                    $userNetwork->locked = '0';
                    $userNetwork->access_token = $access_token;
                    $userNetwork->access_token2 = $refresh_token;
                    
                    $userNetwork->save();
                }

            } else {
                echo 'no access token found';
            }

            return Redirect::intended('authorize');
            
        } else {
            return Redirect::intended('authorize'); // put proper error message that authorization failed
        }
    }
    
    public static function refreshToken() {
        
        $uid = Auth::user()->id;
        $nid = self::YOUTUBE_NETWORK_ID;   
        
        $record = UserNetwork::getRecord($uid, $nid);       
        if (count($record) === 1) {
            
            $refresh_token = $record[0]->access_token2;
            
            if (!empty($refresh_token)) {
                $url = 'https://accounts.google.com/o/oauth2/token';
                $params = array(
                    "client_id" => self::YOUTUBE_CLIENT_ID,
                    "client_secret" => self::YOUTUBE_CLIENT_SECRET,
                    "refresh_token" => $refresh_token,
                    "grant_type" => "refresh_token"
                );

                /* Compile curl call and retrieve access token */
                $curl = curl_init($url);    // we init curl by passing the url
                curl_setopt($curl,CURLOPT_POST,true);   // to send a POST request
                curl_setopt($curl,CURLOPT_POSTFIELDS,$params);   // indicate the data to send
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   // to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   // to stop cURL from verifying the peer's certificate.
                $result = curl_exec($curl);   // to perform the curl session
                curl_close($curl);   // to close the curl session

                $arr = json_decode($result,true); 

                UserNetwork::updateAccessTokens($uid, $nid, $arr['access_token'], $refresh_token);
            }
            
        } else {
            echo 'no access token found';
        }
    }
    
    
    /**
     *Returns a json string of updates related to the account credentials given.
     * */
    public static function retrieveYoutube(){
        
        self::refreshToken();

        $uid = Auth::user()->id;
        $nid = self::YOUTUBE_NETWORK_ID;   
        
        $record = UserNetwork::getRecord($uid, $nid);      

        if (count($record) === 1) {
            $access_token = $record[0]->access_token;
        
            /* Compile url for retrieving all subscriptions for the user */
            $url = 'https://www.googleapis.com/youtube/v3/subscriptions?part=snippet&mine=true&maxResults=50&key='.self::YOUTUBE_SERVER_API_KEY;

            /* Compile curl call and retrieve subscriptions from youtube */
            $curl = curl_init($url);
            $header = array('Authorization: Bearer ' . $access_token);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $subscriptions = curl_exec($curl);
            curl_close($curl);

            $subs = json_decode($subscriptions,true);

            /* Extract the relevant information about the subscriptions */
            $channels = array();
            
            foreach($subs['items'] as $sub){
                array_push(
                        $channels, array(
                            'channel_id' => $sub['snippet']['resourceId']['channelId'],
                            'channel_name' => $sub['snippet']['title'],
                            'channel_thumbnail' => $sub['snippet']['thumbnails']['high'])
                        );
            }
            
            $threeDaysAgo = new DateTime();
            $threeDaysAgo->setTimestamp(strtotime('3 days ago'));
            
            $since_time = $threeDaysAgo->format("Y-m-d\TH:i:s\Z");
            
            /* Get a list of the latest videos from each of the subscription channels */
            foreach($channels as $channel){
                
                $url = 'https://www.googleapis.com/youtube/v3/search?part=snippet&channelId='.$channel['channel_id']
                        .'&maxResults=5&order=date&type=upload&publishedAfter='.$since_time
                        .'&key='.self::YOUTUBE_SERVER_API_KEY;

                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                $videos = curl_exec($curl);
                curl_close($curl);

                $vids = json_decode($videos,true);
                
                foreach ($vids['items'] as $data) {

                    $updateId = Auth::user()->id.'_'.self::YOUTUBE_NETWORK_ID.'_'.$data['id']['videoId'];
                    
                    /* GET THE VIDEO STATISTICS */
                    $videoStatsUrl = 'https://www.googleapis.com/youtube/v3/videos?part=statistics&id='.$data['id']['videoId'].'&key='.self::YOUTUBE_SERVER_API_KEY;

                    /* Compile curl call and retrieve subscriptions from youtube */
                    $curl = curl_init($videoStatsUrl);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    $subscriptions = curl_exec($curl);
                    curl_close($curl);

                    $statistics = json_decode($subscriptions,true);                    
                    $stats = $statistics['items'][0]['statistics'];
                    
                    $like_count = $stats['likeCount'];
                    $dislike_count = $stats['dislikeCount'];
                    $comment_count = $stats['commentCount'];
                    $view_count = $stats['viewCount'];                    
       
                    if (count(Update::withTrashed()->find($updateId)) === 0) {       

                        $update = new Update();

                        $update->id = $updateId; // The unique ID returned by the network with the network ID attached as prefix (ie 1_1243712894219348712). use the constant

                        $update->event_datetime = gmdate("Y-m-d H:i:s", strtotime($data['snippet']['publishedAt'])); // timestamp of when the event happened

                        $update->user_id = Auth::user()->id; // the logged in user. dont change this unless you know what youre doing
                        $update->username = Auth::user()->username; // this too

                        $update->network_id = self::YOUTUBE_NETWORK_ID; // use the constant provided in each integration (I will probably move those constants to a global somewhere) 
                        $update->network_name = 'youtube'; // name of the network.

                        $update->update_type = "video"; // should always be video.

                        $update->profile_image = $channel['channel_thumbnail']['url']; // channel image

                        $update->title1 = $data['snippet']['title'];  // Video title 
                        $update->title2 = NULL; 

                        $update->name1 = $data['snippet']['channelTitle']; // channel title
                        $update->name2 = $data['snippet']['channelId']; // channel id just incase
                        $update->name3 = NULL; 
                        $update->name4 = NULL; 

                        $update->text1 = $data['snippet']['description']; // video description. automatically shortened by youtube
                        $update->text2 = NULL; 
                        $update->text3 = NULL; 
                        $update->text4 = NULL; 

                        $update->media_small_1 = $data['snippet']['thumbnails']['medium']['url']; 
                        $update->media_small_2 = NULL;
                        $update->media_small_3 = NULL;
                        $update->media_small_4 = NULL; 

                        $update->media_large_1 = $data['snippet']['thumbnails']['high']['url'];  
                        $update->media_large_2 = NULL; 
                        $update->media_large_3 = NULL; 
                        $update->media_large_4 = NULL; 

                        $update->source_url = 'http://www.youtube.com/watch?v='.$data['id']['videoId']; // http://www.youtube.com/watch?v= + video ID

                        $update->outside_url1 = NULL; // outside linking url for whatever (news shards, linked stuff, other stuff.. iono)
                        $update->outside_url2 = NULL; 
                        $update->outside_url3 = NULL; 
                        $update->outside_url4 = NULL; 

                        $update->statistic1 = $like_count; 
                        $update->statistic2 = $dislike_count; 
                        $update->statistic3 = $comment_count; 
                        $update->statistic4 = $view_count; 

                        $update->bookmarked = '0'; // defaults to 0. delete this line even... unless you wanna auto bookmark something

                        $update->save();
                        // DEBUG
                        //echo 'saved this shit<br>';
                    } else {

                        $update = Update::withTrashed()->find($updateId);
                        
                        $update->profile_image = $channel['channel_thumbnail']['url']; // channel image

                        // if it already exists, just update the statistics
                        $update->statistic1 = $like_count; 
                        $update->statistic2 = $dislike_count; 
                        $update->statistic3 = $comment_count; 
                        $update->statistic4 = $view_count;  

                        $update->save();

        //                echo 'duplicate. statistics updated.<br>'; //DEBUG
                    }
//                    echo $update;//DEBUG
//                    echo "<br><br><br>**********************<br><br><br>";//DEBUG
                
                }
            }        

        } 
        
    }
    
    public function deauthorizeYoutube(){
                
        $record = UserNetwork::where('user_id', '=', Auth::user()->id)
                    ->where('network_id', '=', self::YOUTUBE_NETWORK_ID)
                    ->take(1);

        $record->delete();

        $updates = Update::where('user_id', '=', Auth::user()->id)
                ->where('network_id', '=', self::YOUTUBE_NETWORK_ID)
                ->get();
        
        foreach ($updates as $update) {
            $update->forceDelete();
        }
        
        return Redirect::to('authorize');

    }    

}