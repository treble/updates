<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TumblrController extends BaseController {
    
    /** @var int network id */
    const TUMBLR_NETWORK_ID   = 6;    
    /** @var string $app_id */
    const TUMBLR_CONSUMER_TOKEN         = "nXKrg09PMfRDsdD6F91UXRRCUYwVrDcm2JLU3zfBSmaalprNTG";
    /** @var string $app_secret */
    const TUMBLR_CONSUMER_SECRET	= "qaMUw8v3KukJnEkofhd9wqt0Z4Yt5f2QoYRNsahhzkxlIi9UuN";
    
    public function authorizeTumblr() {
        
        $callback_url = Config::get('app.base_url').'/authorize/tumblr/redirect';

        $tum_oauth = new TumblrOAuth(self::TUMBLR_CONSUMER_TOKEN, self::TUMBLR_CONSUMER_SECRET);

        $request_token = $tum_oauth->getRequestToken($callback_url);
        
        $token = $request_token['oauth_token'];
        
        Session::put('request_oauth_token', $request_token['oauth_token']); //find more elegant way than session 
        Session::put('request_oauth_token_secret', $request_token['oauth_token_secret']);

        switch ($tum_oauth->http_code) {
          case 200:
              
            $url = $tum_oauth->getAuthorizeURL($token);
              
            header('Location: ' . $url);

            break;
        default:
            // Give an error message
            echo 'Could not connect to Tumblr. Refresh the page or try again later.';
        }
        exit();

    }
    
    public function verifyTumblr() {

        if (!empty($_GET['oauth_verifier'])) {
            
            $oauth = new TumblrOAuth(
                        self::TUMBLR_CONSUMER_TOKEN, 
                        self::TUMBLR_CONSUMER_SECRET, 
                        Session::get('request_oauth_token'),           //find more elegant way than session 
                        Session::get('request_oauth_token_secret')      
                    );

            Session::forget('request_oauth_token');
            Session::forget('request_oauth_token_secret');
            
            /* Get an access token */
            $access_token = $oauth->getAccessToken(Input::get('oauth_verifier'));
            
            $oauthToken = $access_token['oauth_token'];
            $oauthSecret = $access_token['oauth_token_secret'];

            if (!empty($access_token)) {

                $uid = Auth::user()->id;
                $nid = self::TUMBLR_NETWORK_ID;

                $userNetwork = UserNetwork::getRecord($uid, $nid);

                if (count($userNetwork) == 1) {

                    UserNetwork::updateAccessTokens($uid, $nid, $oauthToken, $oauthSecret);

                } else {
                    $userNetwork = new UserNetwork();

                    $userNetwork->user_id = Auth::user()->id;
                    $userNetwork->username = User::find(Auth::user()->id)->username;
                    $userNetwork->network_id = self::TUMBLR_NETWORK_ID;
                    $userNetwork->network_name = Network::find($userNetwork->network_id)->network_name;
                    $userNetwork->locked = '0';
                    $userNetwork->access_token = $oauthToken;
                    $userNetwork->access_token2 = $oauthSecret;

                    $userNetwork->save();
                }
            } 

            return Redirect::to('authorize');

        } else {
            die('sum ting wong');
        }
     
    }
    
    /**
     *Returns a json string of updates related to the account credentials given.
     * */
    public static function retrieveTumblr(){
        
        $uid = Auth::user()->id;
        $nid = self::TUMBLR_NETWORK_ID;   
        
        $record = UserNetwork::getRecord($uid, $nid);   
        
        if (count($record) !== 0) {
        
            $oauthToken = $record[0]->access_token;
            $oauthSecret = $record[0]->access_token2;

            $client = new Tumblr\API\Client(self::TUMBLR_CONSUMER_TOKEN, self::TUMBLR_CONSUMER_SECRET);
            $client->setToken($oauthToken, $oauthSecret);
            
            $options = array(
                'reblog_info' => 'true',
                'limit' => '25'
                );
            $tumbls = $client->getDashboardPosts($options);

            $updates = json_decode(json_encode($tumbls), true);
            
            include_once('debug.php');
            
            if (count($updates['posts']) != 0) {
                foreach($updates['posts'] as $data) {

                    $updateId = Auth::user()->id.'_'.self::TUMBLR_NETWORK_ID.'_'.$data['id'];
                    
                    $profilePic = 'http://api.tumblr.com/v2/blog/'.$data['blog_name'].'.tumblr.com/avatar/128';

                    if (count(Update::withTrashed()->find($updateId)) === 0) {       
                        
                        $update = new Update();

                        $update->id = $updateId; 

                        $update->event_datetime = gmdate("Y-m-d H:i:s", $data['timestamp']); // timestamp of when the event happened

                        $update->user_id = Auth::user()->id; // the logged in user. dont change this unless you know what youre doing
                        $update->username = Auth::user()->username; // this too

                        $update->network_id = self::TUMBLR_NETWORK_ID; // use the constant provided in each integration (I will probably move those constants to a global somewhere) 
                        $update->network_name = 'tumblr'; // name of the network.
                        //
                        // type of update. dependent on each network if network has multiple types.
                        $update->update_type = $data['type']; 

                        $update->profile_image = $profilePic; // url to the profile image. 
                        
                        $reblogged_from = NULL;
                        
                        $data_type = strtolower($data['type']);
                        
                        $blog_name = $data['blog_name'];
                        
                        if (isset($data['reblogged_from_name'])) {
                            $reblogged_from = $data['reblogged_from_name'];
                        }
                        
                        if ($data_type == 'text') {
                            if ($reblogged_from) {
                                $title = $blog_name . ' reposted a blog from ' . $reblogged_from.':';
                            } else {
                                $title = $blog_name . ' posted a blog:';
                            }
                        }  else {
                            // check if data type starts with a vowel or not 
                            $grammar_nazi = preg_match('/^[aeiou]|s\z/i', $data_type)?'an '.$data_type:'a '.$data_type;
                            
                            if ($reblogged_from) {
                                $title = $blog_name . ' reposted '.$grammar_nazi.' from ' . $reblogged_from.':';
                            } else {
                                $title = $blog_name . ' posted '.$grammar_nazi.':';
                            }                           
                        }

                        $update->title1 = $title;  // made static title for this, pretty sure instagram is just picture uploads. who knows. 
                        $update->title2 = NULL; // use full name or username?

                        // not sure.. names i guess. save names here for whatever. or dont, im just a comment not a cop.
                        $update->name1 = $blog_name;
                        $update->name2 = NULL;  
                        $update->name3 = NULL; 
                        $update->name4 = NULL; 
                        
                        
                        // IF PROBLEMS OCCUR WITH FORMATTING: USE strip_tags()
                        $text = array();
                        if (isset($data['title'])&&!empty($data['title'])) {
                            $text[] = $data['title'];
                        } 
                        if (isset($data['id3_title'])&&!empty($data['id3_title'])) {
                            $text[] = $data['id3_title'];
                        } 
                        if (isset($data['source_title'])&&!empty($data['source_title'])) {
                            $text[] = $data['source_title'];
                        }
                        if (isset($data['text'])&&!empty($data['text'])) {
                            $text[] = $data['text'];
                        }
                        if (isset($data['caption'])&&!empty($data['caption'])) {
                            $text[] = $data['caption'];
                        }
                        if (isset($data['body'])&&!empty($data['body'])) {
                            $text[] = $data['body'];
                        }
                        if (isset($data['description'])&&!empty($data['description'])) {
                            $text[] = $data['description'];
                        }
                        if (isset($data['question'])&&!empty($data['question'])) {
                            $text[] = $data['question'];
                        }
                        if (isset($data['answer'])&&!empty($data['answer'])) {
                            $text[] = $data['answer'];
                        }
                        
                        $update->text1 = isset($text[0])?strip_tags($text[0]):NULL;
                        $update->text2 = isset($text[1])?strip_tags($text[1]):NULL;
                        $update->text3 = isset($text[2])?strip_tags($text[2]):NULL;
                        $update->text4 = NULL;
                        
                        if (isset($data['photos'])) {
                            $lastData = end($data['photos'][0]['alt_sizes']);
                            $update->media_small_1 = $lastData['url']; 
                            $update->media_small_2 = NULL;
                            $update->media_small_3 = NULL;
                            $update->media_small_4 = NULL; 

                            $update->media_large_1 = $data['photos'][0]['alt_sizes'][0]['url'];  
                            $update->media_large_2 = NULL; 
                            $update->media_large_3 = NULL; 
                            $update->media_large_4 = NULL;                           
                        }


                        $update->source_url = $data['post_url'];
                        
                        if (isset($data['source_url'])) {
                            $source = $data['source_url'];
                        } else if (isset($data['url'])) {
                            $source = $data['url'];
                        } else {
                            $source = NULL;
                        }
                        
                        $update->outside_url1 = $source; // outside linking url for whatever (news shards, linked stuff, other stuff.. iono)
                        $update->outside_url2 = NULL; 
                        $update->outside_url3 = NULL; 
                        $update->outside_url4 = NULL; 

                        $update->statistic1 = $data['note_count']; // retweets
                        $update->statistic2 = NULL;
                        $update->statistic3 = NULL; 
                        $update->statistic4 = NULL; 

                        $update->bookmarked = '0'; // defaults to 0. delete this line even... unless you wanna auto bookmark something

                        $update->save();

        //                echo 'saved this shit<br>'; //DEBUG
                    } else {

                        $update = Update::withTrashed()->find($updateId);

                        // if it already exists, just update the statistics
                        $update->statistic1 = $data['note_count']; // retweets
                        $update->statistic2 = NULL;
                        $update->statistic3 = NULL; 
                        $update->statistic4 = NULL; 

                        $update->save();

        //                echo 'duplicate. statistics updated.<br>'; //DEBUG
                    }
        //            echo $update;
        //            echo "<br><br><br>**********************<br><br><br>"; //DEBUG
                }
            }
            $time = strtotime('10 minutes ago');
            UserNetwork::updateLastRetrieve($uid, $nid, $time);
        }
        
    }
    
    public function deauthorizeTumblr(){
                
        $record = UserNetwork::where('user_id', '=', Auth::user()->id)
                    ->where('network_id', '=', self::TUMBLR_NETWORK_ID)
                    ->take(1);

        $record->delete();
        
        $updates = Update::where('user_id', '=', Auth::user()->id)
                ->where('network_id', '=', self::TUMBLR_NETWORK_ID)
                ->get();
        
        foreach ($updates as $update) {
            $update->forceDelete();
        }
        
        return Redirect::to('authorize');

    }    

}