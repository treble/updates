<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class NetAuthController extends BaseController {

    public function displayAuthorize() {

        $authorized = UserNetwork::where('user_id', '=', Auth::user()->id)->get();
        
        $user_networks = array();
        
        foreach ($authorized as $authorize) {
            $user_networks[$authorize->network_name] = 1;
        }
                
        return View::make('user.settings.authorize')
                ->with(array('user_networks' => $user_networks));        
    }
    
}