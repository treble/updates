<?php

class RssController extends BaseController
{
    /** @var int network id */
    const RSS_NETWORK_ID = 7;

    public function addRss($link = '')
    {
        if (!empty($link)) {
            $rssFeed = new RssFeed();

            $rssFeed->user_id = Auth::user()->id;
            $rssFeed->rss_link = $link;

            $rssFeed->save();
        } else {
            echo 'Error: no RSS url given.';
        }
    }

    public function removeRss($link = '')
    {
        if (!empty($link)) {
            echo $link;
            die();
        }
    }

    public function getRss()
    {
        echo RssFeed::where('user_id', '=', Auth::user()->id)
            ->orderBy('updated_at', 'desc')
            ->get();
    }

    /**
     *Returns a json string of updates related to the account credentials given.
     * */
    public static function retrieveRss()
    {

    }
}