<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class TwitterController extends BaseController {
    
    /** @var int network id */
    const TWITTER_NETWORK_ID   = 3;    
    /** @var string $app_id */
    const TWITTER_CONSUMER_KEY          = "kL1Tzm3CK5momksdNzDVPw";
    /** @var string $app_secret */
    const TWITTER_CONSUMER_SECRET	= "oGgA1pd5h7jrnx4fuMTD9eQmBh8mlFFEaZzM1FwEcVE";
    
    
    public function authorizeTwitter() {
        
        /* Construct an OAuth module to assist in authentication */
        $oauth = new TwitterOAuth(self::TWITTER_CONSUMER_KEY, self::TWITTER_CONSUMER_SECRET);

        /* Get a request token */
        $request_token = $oauth->getRequestToken(
                    Config::get('app.base_url').'/authorize/twitter/redirect'
                );
        
        Session::put('request_oauth_token', $request_token['oauth_token']); //find more elegant way than session 
        Session::put('request_oauth_token_secret', $request_token['oauth_token_secret']);
        
        if ($oauth->http_code == 200) {
            /* Get authorization from twitter */
            $url = $oauth->getAuthorizeURL($request_token['oauth_token']);
            return Redirect::to($url);
        } else {
            die('Something wrong happened.');
        }
    }
    
    public function verifyTwitter() {

        if (!empty($_GET['oauth_verifier'])) {
            
            $oauth = new TwitterOAuth(
                    self::TWITTER_CONSUMER_KEY, 
                    self::TWITTER_CONSUMER_SECRET, 
                    Session::get('request_oauth_token'),           //find more elegant way than session 
                    Session::get('request_oauth_token_secret')      
                    );

            Session::forget('request_oauth_token');
            Session::forget('request_oauth_token_secret');
            
            /* Get an access token */
            $access_token = $oauth->getAccessToken(Input::get('oauth_verifier'));
            
            $oauthToken = $access_token['oauth_token'];
            $oauthSecret = $access_token['oauth_token_secret'];

            if (!empty($access_token)) {

                $uid = Auth::user()->id;
                $nid = self::TWITTER_NETWORK_ID;

                $userNetwork = UserNetwork::getRecord($uid, $nid);

                if (count($userNetwork) == 1) {

                    UserNetwork::updateAccessTokens($uid, $nid, $oauthToken, $oauthSecret);

                } else {
                    $userNetwork = new UserNetwork();

                    $userNetwork->user_id = Auth::user()->id;
                    $userNetwork->username = User::find(Auth::user()->id)->username;
                    $userNetwork->network_id = self::TWITTER_NETWORK_ID;
                    $userNetwork->network_name = Network::find($userNetwork->network_id)->network_name;
                    $userNetwork->locked = '0';
                    $userNetwork->access_token = $oauthToken;
                    $userNetwork->access_token2 = $oauthSecret;

                    $userNetwork->save();
                }
            } 

            return Redirect::to('authorize');

        } else {
            die('sum ting wong');
        }
     
    }
    
    /**
     *Returns a json string of updates related to the account credentials given.
     * */
    public static function retrieveTwitter(){
        
        $uid = Auth::user()->id;
        $nid = self::TWITTER_NETWORK_ID;   
        
        $record = UserNetwork::getRecord($uid, $nid);   
        
        if (count($record) !== 0) {
        
            $oauthToken = $record[0]->access_token;
            $oauthSecret = $record[0]->access_token2;

            $twitteroauth = new TwitterOAuth(
                    self::TWITTER_CONSUMER_KEY, 
                    self::TWITTER_CONSUMER_SECRET, 
                    $oauthToken, 
                    $oauthSecret);

            /* Retrieve tweets from the user's home timeline */
            $limit = $twitteroauth->get('application/rate_limit_status');
            $limit = $limit->resources->statuses;
            $limit = json_decode(json_encode($limit), true);
            $limit = $limit['/statuses/home_timeline']['remaining'];

            if ($limit > 1) {
                $tweets = $twitteroauth->get('statuses/home_timeline');
                $updates = json_decode(json_encode($tweets), true);

                include_once('debug.php');

                if (count($updates) != 0) {
                    foreach($updates as $data) {

                        $updateId = Auth::user()->id.'_'.self::TWITTER_NETWORK_ID.'_'.$data['id_str'];   

                        if (count(Update::withTrashed()->find($updateId)) === 0) {       

                            $update = new Update();

                            $update->id = $updateId; 

                            $update->event_datetime = gmdate("Y-m-d H:i:s", strtotime($data['created_at'])); // timestamp of when the event happened

                            $update->user_id = Auth::user()->id; // the logged in user. dont change this unless you know what youre doing
                            $update->username = Auth::user()->username; // this too

                            $update->network_id = self::TWITTER_NETWORK_ID; // use the constant provided in each integration (I will probably move those constants to a global somewhere) 
                            $update->network_name = 'twitter'; // name of the network.
                            //
                            // type of update. dependent on each network if network has multiple types.
                            $update->update_type = 'tweet'; 

                            $update->profile_image = str_replace('_normal','_bigger', $data['user']['profile_image_url']); // url to the profile image. 

                            $update->title1 = $data['user']['name'].' tweeted';  // made static title for this, pretty sure instagram is just picture uploads. who knows. 
                            $update->title2 = NULL; // use full name or username?

                            // not sure.. names i guess. save names here for whatever. or dont, im just a comment not a cop.
                            $update->name1 = $data['user']['name']; 
                            $update->name2 = $data['user']['screen_name']; 
                            $update->name3 = NULL; 
                            $update->name4 = NULL; 

                            $update->text1 = $data['text']; 
                            $update->text2 = NULL; 
                            $update->text3 = NULL; 
                            $update->text4 = NULL; 

                            if (isset($data['entities']['media'][0]['media_url'])) {
                                $update->media_small_1 = $data['entities']['media'][0]['media_url']; 
                            } else {
                                $update->media_small_1 = NULL; 
                            }
                            $update->media_small_2 = NULL;
                            $update->media_small_3 = NULL;
                            $update->media_small_4 = NULL; 

                            $update->media_large_1 = NULL;  
                            $update->media_large_2 = NULL; 
                            $update->media_large_3 = NULL; 
                            $update->media_large_4 = NULL; 

                            // twitter uses the format: https://twitter.com/*screen_name*/status/*id*
                            $update->source_url = "https://twitter.com/".$data['user']['screen_name']."/status/".$data['id_str']; 
                            $update->outside_url1 = NULL; // outside linking url for whatever (news shards, linked stuff, other stuff.. iono)
                            $update->outside_url2 = NULL; 
                            $update->outside_url3 = NULL; 
                            $update->outside_url4 = NULL; 

                            $update->statistic1 = $data['retweet_count']; // retweets
                            $update->statistic2 = $data['favorite_count']; // favorites
                            $update->statistic3 = NULL; 
                            $update->statistic4 = NULL; 

                            $update->bookmarked = '0'; // defaults to 0. delete this line even... unless you wanna auto bookmark something

                            $update->save();

            //                echo 'saved this shit<br>'; //DEBUG
                        } else {

                            $update = Update::withTrashed()->find($updateId);
                            
                            $update->profile_image = str_replace('_normal','_bigger', $data['user']['profile_image_url']); // url to the profile image. 

                            // if it already exists, just update the statistics
                            $update->statistic1 = $data['retweet_count']; // retweets
                            $update->statistic2 = $data['favorite_count']; // favorites
                            $update->statistic3 = NULL; 
                            $update->statistic4 = NULL; 

                            $update->save();

            //                echo 'duplicate. statistics updated.<br>'; //DEBUG
                        }
            //            echo $update;
            //            echo "<br><br><br>**********************<br><br><br>"; //DEBUG
                    }
                }
                $time = strtotime('10 minutes ago');
                UserNetwork::updateLastRetrieve($uid, $nid, $time);
            }
        
        }
        
    }
    
    public function deauthorizeTwitter(){
                
        $record = UserNetwork::where('user_id', '=', Auth::user()->id)
                    ->where('network_id', '=', self::TWITTER_NETWORK_ID)
                    ->take(1);

        $record->delete();
        
        $updates = Update::where('user_id', '=', Auth::user()->id)
                ->where('network_id', '=', self::TWITTER_NETWORK_ID)
                ->get();
        
        foreach ($updates as $update) {
            $update->forceDelete();
        }
        
        return Redirect::to('authorize');

    }    

}