<?php
abstract class OAuthBase
{
    abstract function getRequestToken($returnURL);

    abstract function getAuthorizeURL($token);

    abstract function getAccessToken($oauthVerifier);

    abstract function get($url, $parameters = null);
}