<?php

class UpdatesController extends BaseController {

    public function returnUpdates($network = 'all', $limit = 42, $offset = 0) { 
        
        if ($network === 'all') {
          $updates = Update::where('user_id', '=', Auth::user()->id)
                ->orderBy('event_datetime', 'desc')
                ->take($limit)
                ->skip($offset)
                ->get();
        } else {
          $updates = Update::where('user_id', '=', Auth::user()->id)
                ->where('network_name', '=', $network)
                ->orderBy('event_datetime', 'desc')
                ->take($limit)
                ->skip($offset)
                ->get();
        }
          
        echo $updates;
    }
    
    public function deleteUpdate($id) {
        
        if (!empty($id)) {
            $update = Update::find($id);
            $update->delete();
        }

        
    }
  
}