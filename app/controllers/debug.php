<?php
function print_var($aVals, $varName=null)
{
    echo "<ul>";
    if(isset($varName)){
        echo "<strong>".$varName."</strong>";
    }
    foreach ($aVals as $key => $val) {
        if (is_array($val)) {
            echo "<li><strong>{$key}-array</strong>";
            print_var($val);
            echo "</li>";
        } else if (is_object($val)) {
            echo "<li><strong>{$key}-stdclass</strong>";
            print_var((array)$val);
            echo "</li>";
        } else {
            if($val == null){
                echo "<li>{$key}: null</li>";
            } else {
                echo "<li>{$key}: {$val}</li>";
            }
        }
    }
    echo "</ul>";
}