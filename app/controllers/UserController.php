<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class UserController extends BaseController {

    public function displayHome() {
        $authorized = UserNetwork::where(
                'user_id', '=', Auth::user()->id)
                ->get();
        
        $user_networks = array();
        
        foreach ($authorized as $authorize) {
            $user_networks[$authorize->network_name] = 1;
        }        

        return View::make('user.index')
                ->with(array('user_networks' => $user_networks));
        
    }
    
    public function massRetrieve() {
        
        $authorized = UserNetwork::where('user_id', '=', Auth::user()->id)->get();
        
        $user_networks = array();

        foreach ($authorized as $authorize) {
            if ($authorize->locked == '0') {
                $user_networks[$authorize->network_name] = 1;
            }
        }
        
        if (isset($user_networks['facebook'])) {
            FacebookController::retrieveFacebook();
        }
        
        if (isset($user_networks['instagram'])) {
            InstagramController::retrieveInstagram();

        }      
        
        if (isset($user_networks['twitter'])) {
            TwitterController::retrieveTwitter();
        }        
        
        if (isset($user_networks['youtube'])) {
            YoutubeController::retrieveYoutube();
        }
        
        if (isset($user_networks['tumblr'])) {
            TumblrController::retrieveTumblr();
        } 
    }
} 