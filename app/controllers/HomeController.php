<?php

class HomeController extends BaseController {

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */

    public function getLogin() {
        if (Auth::user()) {
            return Redirect::to('user');
        } else {
            return View::make('home.login');
        }
    }
    
    public function postLogin() 
    {
         $input = Input::all();

         $rules = array(
             'email' => 'required',
             'password' => 'required'
         );
         
         $v = Validator::make($input, $rules);
         
         if($v->fails()){
             return Redirect::to('login')->withErrors($v);
         } else {
             
             $credentials = array(
                 'email' => $input['email'],
                 'password' => $input['password']
             );
             
             if(Auth::attempt($credentials)){
                 return Redirect::intended('user');
             } else {
                return Redirect::to('login');
             }
         }
    }

    public function logout() {
        Auth::logout();
        return Redirect::to('/');
    }
    
    public function getRegister() {
        if (Auth::user()) {
            return Redirect::to('user');
        } else {
            return View::make('home.register');
        }
    }
    
    public function postRegister()
    {
        $input = Input::all();
        
        $rules = array(
            'username' => 'required|unique:users',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:8',
            'confirm_password' => 'required|same:password'
            );
        
        $v = Validator::make($input, $rules);
        
        if($v->passes())
        {
            $password = $input['password'];
            $password = Hash::make($password);
            
            $user = new User();
            $user->username = $input['username'];
            $user->email = $input['email'];
            $user->password = $password;
            $user->save();
            
            return Redirect::to('login')->with(array('success' => 'Registration Successful. Please Login.'));
        } else {
            return Redirect::to('register')->withInput()->withErrors($v);
        }
    }

}