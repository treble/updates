<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class InstagramController extends BaseController {

    const INSTAGRAM_NETWORK_ID = 2;    
    
    /** @var string $clientKey application client key */
    const INSTAGRAM_CLIENT_KEY = '05c8eb93f4c04217b81e7929d8762700';
    
    const INSTAGRAM_CLIENT_SECRET = '1b4ce72b4040425fa66cb4b45c6d03b0';
    
    public function authorizeInstagram() {

        /** @var string $callback callback method */
        $callback = Config::get('app.base_url').'/authorize/instagram/redirect';
        /* Compile url to authorize our application with instagram */
        $url = "https://instagram.com/oauth/authorize/?client_id=" . self::INSTAGRAM_CLIENT_KEY . "&redirect_uri=" . $callback . "&response_type=code";
        return Redirect::to($url);        
        
    }
    
    public function verifyInstagram() {

        /* Authorization from instagram should return an oauth code */
        if($_GET['code']) {

            /** @var string $code unique code from instagram */
            $code = $_GET['code'];

            /** @var string $url url for retrieving an instagram access token */
            $url = "https://api.instagram.com/oauth/access_token";

            /** @var array $access_token_parameters parameters required for retreiving an access token */
            $access_token_parameters = array(
                'client_id'                =>     self::INSTAGRAM_CLIENT_KEY, /* client_id for our application */
                'client_secret'            =>     self::INSTAGRAM_CLIENT_SECRET, /* client_secret for our application */
                'grant_type'               =>     'authorization_code', /* constant */
                'redirect_uri'             =>     Config::get('app.base_url').'/authorize/instagram/redirect',
                'code'                     =>     $code
            );

            /* Compile curl call and retrieve access token */
            $curl = curl_init($url);    // we init curl by passing the url
            curl_setopt($curl,CURLOPT_POST,true);   // to send a POST request
            curl_setopt($curl,CURLOPT_POSTFIELDS,$access_token_parameters);   // indicate the data to send
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);   // to return the transfer as a string of the return value of curl_exec() instead of outputting it out directly.
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);   // to stop cURL from verifying the peer's certificate.
            $result = curl_exec($curl);   // to perform the curl session
            curl_close($curl);   // to close the curl session

            $arr = json_decode($result,true);
            if ( isset($arr['access_token'])) {
                $access_token = $arr['access_token'];
            } else {
                unset($access_token);
            }
        }
        
        if (isset($access_token)) {
            
                $uid = Auth::user()->id;
                $nid = self::INSTAGRAM_NETWORK_ID;
                
                $userNetwork = UserNetwork::getRecord($uid, $nid);
           
                if (count($userNetwork) == 1) {
                    
                    UserNetwork::updateAccessTokens($uid, $nid, $access_token);
                    
                } else {
                    $userNetwork = new UserNetwork();
                
                    $userNetwork->user_id = Auth::user()->id;
                    $userNetwork->username = User::find(Auth::user()->id)->username;
                    $userNetwork->network_id = self::INSTAGRAM_NETWORK_ID;
                    $userNetwork->network_name = Network::find($userNetwork->network_id)->network_name;
                    $userNetwork->locked = '0';
                    $userNetwork->access_token = $access_token;
                    
                    $userNetwork->save();
                }
        } 
        
        return Redirect::to('authorize');

    }
    
    /**
     *Returns a json string of updates related to the account credentials given.
     * */
    public static function retrieveInstagram(){
                
        $uid = Auth::user()->id;
        $nid = self::INSTAGRAM_NETWORK_ID;   
        
        $record = UserNetwork::getRecord($uid, $nid);
        
        if (count($record) !== 0) {

            $access_token = $record[0]->access_token;

            /* Compile url for retrieving pictures from a user's feed */
            $pictureURL = 'https://api.instagram.com/v1/users/self/feed?access_token='.$access_token;

            /* Compile curl call and retrieve pictures from instagram */
            $curl = curl_init($pictureURL);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            $pictures = curl_exec($curl);
            curl_close($curl);

            $feed = json_decode($pictures,true);

//            include_once('debug.php');
//            print_r($feed);
//            die();

            if (count($feed['data']) != 0) {
                foreach($feed['data'] as $data) {        

                    $updateId = Auth::user()->id.'_'.self::INSTAGRAM_NETWORK_ID.'_'.$data['id'];

                    if (count(Update::withTrashed()->find($updateId)) === 0) {       

                        $update = new Update();

                        $update->id = $updateId; // The unique ID returned by the network with the network ID attached as prefix (ie 1_1243712894219348712). use the constant

                        $update->event_datetime = gmdate("Y-m-d H:i:s", $data['created_time']); // timestamp of when the event happened

                        $update->user_id = Auth::user()->id; // the logged in user. dont change this unless you know what youre doing
                        $update->username = Auth::user()->username; // this too

                        $update->network_id = self::INSTAGRAM_NETWORK_ID; // use the constant provided in each integration (I will probably move those constants to a global somewhere) 
                        $update->network_name = 'instagram'; // name of the network.

                        $update->update_type = $data['type']; // type of update. dependent on each network if network has multiple types.

                        $update->profile_image = $data['user']['profile_picture']; // url to the profile image. 

                        $update->title1 = $data['user']['full_name'].' has uploaded a picture';  // made static title for this, pretty sure instagram is just picture uploads. who knows. 
                        $update->title2 = NULL;

                        $update->name1 = $data['user']['username']; // not sure.. names i guess. save names here for whatever. or dont, im just a comment not a cop.
                        $update->name2 = $data['user']['full_name']; 
                        $update->name3 = NULL; 
                        $update->name4 = NULL; 

                        $update->text1 = $data['caption']['text']; 
                        $update->text2 = NULL; 
                        $update->text3 = NULL; 
                        $update->text4 = NULL; 

                        $update->media_small_1 = $data['images']['thumbnail']['url']; 
                        $update->media_small_2 = NULL;
                        $update->media_small_3 = NULL;
                        $update->media_small_4 = NULL; 

                        $update->media_large_1 = $data['images']['standard_resolution']['url'];  
                        $update->media_large_2 = NULL; 
                        $update->media_large_3 = NULL; 
                        $update->media_large_4 = NULL; 

                        $update->source_url = $data['link']; // for the "view on blah*"  * = network name | cant be null

                        $update->outside_url1 = NULL; // outside linking url for whatever (news shards, linked stuff, other stuff.. iono)
                        $update->outside_url2 = NULL; 
                        $update->outside_url3 = NULL; 
                        $update->outside_url4 = NULL; 

                        $update->statistic1 = $data['likes']['count'];  // the count of likes
                        $update->statistic2 = $data['comments']['count']; // the count of comments
                        $update->statistic3 = NULL; 
                        $update->statistic4 = NULL; 

                        $update->bookmarked = '0'; // defaults to 0. delete this line even... unless you wanna auto bookmark something

                        $update->save();
                        // DEBUG
                        //echo 'saved this shit<br>';
                    } else {

                        $update = Update::withTrashed()->find($updateId);
                        
                        $update->profile_image = $data['user']['profile_picture']; // url to the profile image. 

                        // if it already exists, just update the statistics
                        $update->statistic1 = $data['likes']['count'];  // the count of likes
                        $update->statistic2 = $data['comments']['count']; // the count of comments
                        $update->statistic3 = NULL; 
                        $update->statistic4 = NULL; 

                        $update->save();

        //                echo 'duplicate. statistics updated.<br>'; //DEBUG
                    }
        //            echo $update;//DEBUG
        //            echo "<br><br><br>**********************<br><br><br>";//DEBUG
                }
            }

            $time = strtotime('10 minutes ago');
            UserNetwork::updateLastRetrieve($uid, $nid, $time);
        }

    }
    
    public function deauthorizeInstagram(){
                
        $record = UserNetwork::where('user_id', '=', Auth::user()->id)
                    ->where('network_id', '=', self::INSTAGRAM_NETWORK_ID)
                    ->take(1);

        $record->delete();
        
        $updates = Update::where('user_id', '=', Auth::user()->id)
                ->where('network_id', '=', self::INSTAGRAM_NETWORK_ID)
                ->get();
        
        foreach ($updates as $update) {
            $update->forceDelete();
        }
        
        return Redirect::to('authorize');

    }
}