@extends('master')

@section('content')

<div class='row'>
    <div class='col-md-3 col-md-offset-4'>
        <div class='well'>
            <legend>Login:</legend>
            {{ Form::open(array('url' => 'login', 'class' => 'form-group')) }}
            @if($errors->any())
            <div class='alert alert-danger'>
                <a href='#' class='close' data-dismiss='alert'>&times;</a>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </div>
            @endif
            @if(Session::get('success'))
            <div class='alert alert-success'>
                <a href='#' class='close' data-dismiss='alert'>&times;</a>
                {{ Session::get('success') }}
            </div>
            @endif
            {{ Form::text('email', '', array('placeholder' => 'Email', 'class' => 'form-control')) }}<br/>
            {{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control')) }}<br/>
            {{ Form::submit('Login', array('class' => 'btn btn-success')) }}
            {{ HTML::link('register', 'Register', array('class' => 'btn btn-primary')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop