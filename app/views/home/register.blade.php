@extends('master')

@section('content')

<div class='row'>
    <div class='col-md-3 col-md-offset-4'>
        <div class='well'>
            <legend>Please Register:</legend>
            {{ Form::open(array('url' => 'register', 'class' => 'form-group')) }}
            @if($errors->any())
            <div class='alert alert-danger'>
                <a href='#' class='close' data-dismiss='alert'>&times;</a>
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </div>
            @endif
            {{ Form::text('username', '', array('placeholder' => 'Username', 'class' => 'form-control')) }}<br/>
            {{ Form::text('email', '', array('placeholder' => 'Email', 'class' => 'form-control')) }}<br/>
            {{ Form::password('password', array('placeholder' => 'Password', 'class' => 'form-control')) }}<br/>
            {{ Form::password('confirm_password', array('placeholder' => 'Confirm Password', 'class' => 'form-control')) }}<br/>
            {{ Form::submit('Register', array('class' => 'btn btn-success')) }}
            {{ HTML::link('login', 'Cancel', array('class' => 'btn btn-danger')) }}
            {{ Form::close() }}
        </div>
    </div>
</div>

@stop