<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Upd8s</title>
    {{ HTML::script('http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js') }}
    {{ HTML::script('packages/assets/js/bootstrap.min.js') }}
    {{ HTML::script('packages/assets/js/jquery.popupoverlay.js') }}
    {{ HTML::script('packages/assets/js/jquery.updates.wookmark.js') }}
    {{ HTML::script('packages/assets/js/tiles.js') }}
    {{ HTML::script('packages/assets/js/popupCarousel.js') }}
    {{ HTML::script('packages/assets/js/jquery.bpopup.js') }}
    {{ HTML::script('packages/assets/js/jquery.featureCarousel.js') }}
    {{ HTML::script('packages/assets/js/jquery.timeago.js') }}
    {{ HTML::script('packages/assets/js/jquery.imagesloaded.js') }}
    {{ HTML::style('packages/assets/css/bootstrap.css') }}
    {{ HTML::style('packages/assets/css/reset.css') }}
    {{ HTML::style('packages/assets/css/main.css') }}
    {{ HTML::style('packages/assets/css/style.css') }}
    {{ HTML::style('packages/assets/css/filters.css') }}
    {{ HTML::style('packages/assets/css/tiles.css') }}
    {{ HTML::style('packages/assets/css/feature-carousel.css') }}
    {{ HTML::style('packages/assets/css/popupCarousel.css') }}
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="col-md-10 col-md-offset-1">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <img src="packages/assets/images/logo.png" class="img-rounded navbar-left small-logo">
            <a class="navbar-brand" href="#">Upd8s</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('user') }}"><b class="glyphicon glyphicon-home"></b></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-cog"></b></a>
                    <ul class="dropdown-menu">
                        @if(Auth::user())
                        <li>{{ HTML::link('logout', 'Logout') }}</li>
                        <li>{{ HTML::link('authorize', 'Authorize Networks') }}</li>
                        @else
                        <li>{{ HTML::link('login', 'Login') }}</li>
                        @endif
                        <li class="divider"></li>
                        <li class="dropdown-header">Nav header</li>
                        <li><a href="#">Separated link</a></li>
                        <li><a href="#">Some other shit</a></li>
                    </ul>
                </li>
            </ul>
            <ul id="filters" class="nav navbar-nav navbar-right">
                @if(Auth::user() && Request::is('user') )
                    @if (isset($user_networks['facebook']))
                    <li data-filter="facebook" class="active"><a href="#"><img src="packages/assets/icons/facebook-icon.png" class="img-rounded small-nav-icon"></a></li>
                    @endif
                    @if (isset($user_networks['twitter']))
                    <li data-filter="twitter" class="active"><a href="#"><img src="packages/assets/icons/twitter-icon.png" class="img-rounded small-nav-icon"></a></li>
                    @endif
                    @if (isset($user_networks['youtube']))
                    <li data-filter="youtube" class="active"><a href="#"><img src="packages/assets/icons/youtube-icon.png" class="img-rounded small-nav-icon"></a></li>
                    @endif
                    @if (isset($user_networks['instagram']))
                    <li data-filter="instagram" class="active"><a href="#"><img src="packages/assets/icons/instagram-icon.png" class="img-rounded small-nav-icon"></a></li>
                    @endif
                    @if (isset($user_networks['tumblr']))
                    <li data-filter="tumblr" class="active"><a href="#"><img src="packages/assets/icons/tumblr-icon.png" class="img-rounded small-nav-icon"></a></li>
                    @endif
                    @if (isset($user_networks['pinterest']))
                    <li data-filter="pinterest" class="active"><a href="#"><img src="packages/assets/icons/pinterest-icon.png" class="img-rounded small-nav-icon"></a></li>
                    @endif
                    @if (isset($user_networks['rss']))
                    <li data-filter="rss" class="active"><a href="#"><img src="packages/assets/icons/rss-icon.png" class="img-rounded small-nav-icon"></a></li>
                    @endif
                @endif
            </ul>


        </div><!--/.nav-collapse -->
    </div>
</div>
@if (Request::is('user') && count($user_networks) == 0 )
<div id="updates_messages" class="alert alert-danger col-md-10 col-md-offset-1">
    You have no Authorized Networks. Please proceed to the {{ HTML::link('authorize', 'Authorization Page') }} and authorize some networks.
</div>
@endif
<div class='row-fluid'>
    @yield('content')
</div>

</body>

</html>