@extends('master')

@section('content')

<div class='col-md-2 col-md-offset-1 well'>
    <h3><b class="glyphicon glyphicon-cog"></b> Settings:</h3><hr>
    <ul class='nav nav-stacked'>
        <li>
            <h4>Network Authorization</h4>
        </li>
    </ul>
</div>
<div class='col-md-8'>
    @yield('settings-content')
</div>
  
@stop