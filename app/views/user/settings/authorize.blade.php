@extends('user.settings.master')

@section('settings-content')

    <div class='container col-md-11 col-md-offset-1 well'>
        <div class='col-md-12'>
            <h4><b class="glyphicon glyphicon-lock"></b> The Following Networks are Available for Authorization:</h4><hr>
        </div>
        <table id='auth-table' class="col-md-12">
            <tr id="facebook-row">
                <td class="col-md-10" >
                    <a href="#"><img src="packages/assets/icons/facebook-icon.png" class="img-rounded small-nav-icon" style='margin-right: 10px;'></a>
                    Authorize Facebook:<hr class='settings-hr' style='border-color:  #EEEEEE; margin:2px 0;  border-width: 2px 0;'>
                </td>
                <td class="col-md-2">
                    @if (isset($user_networks['facebook']))
                    <a href="{{ URL::to('deauthorize/facebook') }}" class='auth-page-btn btn btn-success col-md-12 deauth-btn'><b class='glyphicon glyphicon-ok'></b>Authorized</a>
                    @else
                    {{ HTML::link('authorize/facebook', 'Authorize', array('class' => 'auth-page-btn btn btn-primary col-md-12')) }}
                    @endif
                </td>
            </tr>
            <tr id="instagram-row">
                <td class="col-md-10" >
                    <a href="#"><img src="packages/assets/icons/instagram-icon.png" class="img-rounded small-nav-icon" style='margin-right: 10px;'></a>
                    Authorize Instagram:<hr class='settings-hr' style='border-color:  #EEEEEE; margin:2px 0;  border-width: 2px 0;'>
                </td>
                <td class="col-md-2">
                    @if (isset($user_networks['instagram']))
                    <a href="{{ URL::to('deauthorize/instagram') }}" class='auth-page-btn btn btn-success col-md-12 deauth-btn'><b class='glyphicon glyphicon-ok'></b>Authorized</a>
                    @else
                    {{ HTML::link('authorize/instagram', 'Authorize', array('class' => 'auth-page-btn btn btn-primary col-md-12')) }}
                    @endif
                </td>
            </tr>
            <tr id="youtube-row">
                <td class="col-md-10" >
                    <a href="#"><img src="packages/assets/icons/youtube-icon.png" class="img-rounded small-nav-icon" style='margin-right: 10px;'></a>
                    Authorize Youtube:<hr class='settings-hr' style='border-color:  #EEEEEE; margin:2px 0;  border-width: 2px 0;'>
                </td>
                <td class="col-md-2">
                    @if (isset($user_networks['youtube']))
                    <a href="{{ URL::to('deauthorize/youtube') }}" class='auth-page-btn btn btn-success col-md-12 deauth-btn'><b class='glyphicon glyphicon-ok'></b>Authorized</a>
                    @else
                    {{ HTML::link('authorize/youtube', 'Authorize', array('class' => 'auth-page-btn btn btn-primary col-md-12')) }}
                    @endif
                </td>
            <tr id="twitter-row">
                <td class="col-md-10" >
                    <a href="#"><img src="packages/assets/icons/twitter-icon.png" class="img-rounded small-nav-icon" style='margin-right: 10px;'></a>
                    Authorize Twitter:<hr class='settings-hr' style='border-color:  #EEEEEE; margin:2px 0;  border-width: 2px 0;'>
                </td>
                <td class="col-md-2">
                    @if (isset($user_networks['twitter']))
                    <a href="{{ URL::to('deauthorize/twitter') }}" class='auth-page-btn btn btn-success col-md-12 deauth-btn'><b class='glyphicon glyphicon-ok'></b>Authorized</a>
                    @else
                    {{ HTML::link('authorize/twitter', 'Authorize', array('class' => 'auth-page-btn btn btn-primary col-md-12')) }}
                    @endif
                </td>
            </tr>
            <tr id="tumblr-row">
                <td class="col-md-10" >
                    <a href="#"><img src="packages/assets/icons/tumblr-icon.png" class="img-rounded small-nav-icon" style='margin-right: 10px;'></a>
                    Authorize Tumblr:<hr class='settings-hr' style='border-color:  #EEEEEE; margin:2px 0;  border-width: 2px 0;'>
                </td>
                <td class="col-md-2">
                    @if (isset($user_networks['tumblr']))
                    <a href="{{ URL::to('deauthorize/tumblr') }}" class='auth-page-btn btn btn-success col-md-12 deauth-btn'><b class='glyphicon glyphicon-ok'></b>Authorized</a>
                    @else
                    {{ HTML::link('authorize/tumblr', 'Authorize', array('class' => 'auth-page-btn btn btn-primary col-md-12')) }}
                    @endif
                </td>
            </tr>
            <tr id="rss-row">
                <td class="col-md-10">
                    <a href="#"><img src="packages/assets/icons/rss-icon.png" class="img-rounded small-nav-icon" style='margin-right: 10px;'></a>
                    Authorize RSS:<hr class='settings-hr' style='border-color:  #EEEEEE; margin:2px 0;  border-width: 2px 0;'>
                </td>
                <td class="col-md-2">
                    <div id="show-feeds" data-showing="false" class="auth-page-btn btn btn-primary col-md-12">Show feeds</div>
                </td>
            </tr>
            <!--            <tr id="rss-form">-->
            <!--                <td class="col-md-10">-->
            <!--                    <input id="rss-add-input" class="form-control" placeholder="RSS feed url"  type="text" name="link" />-->
            <!--                </td>-->
            <!--                <td class="col-md-2">-->
            <!--                    <div id="add-feed" data-showing="false" class="auth-page-btn btn btn-primary col-md-12">Add</div>-->
            <!--                </td>-->
            <!--            </tr>-->
            <tr id="rss-display-feeds-row">
                <td class="col-md-12" colspan="2">
                    <p>Feeds that you have registered: </p>
                    <ul id="rss-display-feeds">
                        <li><div>okayokayokayokayokayokay</div></li>
                    </ul>
                </td>
        </table>

    </div>
<script type="text/javascript">
    $("#rss-form").hide();
    $("#rss-display-feeds-row").hide();
    $("#show-feeds").data('showing', 'false');

    $(document).ready(function(){
        $('.deauth-btn').hover(function () {
            $(this).html('<b class="glyphicon glyphicon-remove"></b>Deauthorize');
            $(this).removeClass('btn-success');
            $(this).addClass('btn-danger');
            $(this).addClass('auth-page-btn');
        }, function() {
            $(this).html('<b class="glyphicon glyphicon-ok"></b>Authorized');
            $(this).removeClass('btn-danger');
            $(this).addClass('btn-success');
            $(this).addClass('auth-page-btn');
        });

        $("#add-feed").click(function(){
            var link = $("#rss-add-input").val();
            $.ajax({
                url: '{{ Config::get('app.base_url') }}/addrss/'+link
        });
    });

    function displayFeeds(data) {
        var obj = JSON.parse(data);
        for(var i = 0; i < obj.length; i++){
            $("#rss-display-feeds").append(makeFeedRow(obj[i].rss_link));
        }
    }

    function makeFeedRow(rssFeed){
        //<li><a href="{{ url('user') }}"><b class="glyphicon glyphicon-home"></b></a></li>
//        return '<li><div>'+rssFeed+'</div><div class="glyphicon glyphicon-pencil rss-edit"></div><div class="glyphicon glyphicon-remove rss=delete"></div></li>'
        return '<li><div>'+rssFeed+'</div></li>'
    }

    $("#show-feeds").click(function(){
        if($(this).data('showing') === 'false'){
            $(this).data('showing', 'true');
            $(this).html('Go back');
            $(this).css('background-color', '#5cb85c');
            $(this).css('border-color', '#4cae4c');
            $("#facebook-row").hide();
            $("#instagram-row").hide();
            $("#youtube-row").hide();
            $("#twitter-row").hide();
            $("#tumblr-row").hide();
            $("#rss-form").show();
            $("#rss-display-feeds-row").show();
            $.ajax({
                url: '{{ Config::get('app.base_url') }}/getrss',
                success: displayFeeds
        });
    } else {
        $(this).data('showing', 'false');
        $(this).html('Show feeds');
        $(this).css('background-color', '#428bca');
        $(this).css('border-color', '#357ebd');
        $("#facebook-row").show();
        $("#instagram-row").show();
        $("#youtube-row").show();
        $("#twitter-row").show();
        $("#tumblr-row").show();
        $("#rss-form").hide();
        $("#rss-display-feeds-row").hide();
    }
    });

    });

</script>
@stop