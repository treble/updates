@extends('master')

@section('content')
<div class="makeFullScreen">
   <div class="carousel-container">
       <div id="carousel"></div>
       <span class="button b-close"><span>X</span></span>
<!--       <div id="carousel-close"><button onClick="___">Close Popup Window</button></div>-->
       <div id="carousel-left"><img src="packages/assets/images/arrow-left.png" /></div>
       <div id="carousel-right"><img src="packages/assets/images/arrow-right.png" /></div>
    </div>    
    
</div>

<div class='col-md-12'>
    <br/>
    <div id="dvLoading"></div>
    <div id="main" role="main">

        <ul id="tiles"></ul>
<!--        <div id="loading-circle"></div>-->       
    </div>
</div>

<script type="text/javascript">

    var NUMBER_CARDS = 50;
    var curActiveFilter;
    
    (function ($) {      
        var handler = null,
            filters = null,
            page = 1,
            isLoading = false,
            apiURL = 'user/return';
        var idOfNewest = '';
        var eventTimeOfNewest = '';
        /* Timer only active when $(window).scrollTop == 0 */
        var returnTimer;
        // Prepare layout options.
        var options = {
            autoResize: true, // This will auto-update the layout when the browser window is resized.
            container: $('#main'), // Optional, used for some extra CSS styling
            //container: $('#main'),
            offset: 30, // Optional, the distance between grid items
            itemWidth: 260 // Optional, the width of a grid item
//            fillEmptySpace: true
        };

        /**
         * When scrolled all the way to the bottom, add more tiles.
         */
        
        function onScroll(event) {
            // Only check when we're not still waiting for data.
            if(!isLoading) {
                // Check if we're within 100 pixels of the bottom edge of the broser window.
                var closeToBottom = ($(window).scrollTop() + $(window).height() > $(document).height() - 100);
                if(closeToBottom) {
                    loadOldData();
                }
                var closeToTop = ($(window).scrollTop() == 0);
                if(closeToTop) {
                    loadNewData();
                    returnTimer = setInterval(loadNewData, 20000);
                } else {
                    clearInterval(returnTimer);
                }
            }
        };

        /**
         * Refreshes the layout.
         */
        function applyLayout() {
            options.container.imagesLoaded(function() {
                // Create a new layout handler when images have loaded.
                handler = $('#tiles li');
                handler.wookmark(options);
            });
        };
        //CLOSE FUNCTION
        function closeTile(node) {
            id = node.id;
            div_id = node.parentNode.id;
            $('#'+id).removeClass('active');
            $('#'+id).addClass('inactive');
            $('#'+div_id).remove();
            var db_id = id.substring(13); // length of prefix: "contentItems_"
            //console.log(db_id);
            $.ajax({
                url: 'user/delete/' + db_id, 
            });
        };
        
        $(document).on('click', '#closeIcon', function(e){
            closeTile(this.parentNode.parentNode);
            applyLayout();
        });
        
        //FULLSCREEN FUNCTION
        $(document).on('click', '#fullScreenIcon', function(e){
            var id = this.parentNode.parentNode.id;
            id = id.substring(13); // length of prefix: "contentItems_"
            id = 'fullScreen_' + id;
//            $('#'+id).popup('show');
            
            //$('#'+id).bPopup();

            $('.makeFullScreen').bPopup({
                modalClose: false,
                positionStyle: 'fixed' //'fixed' or 'absolute'
            });
            //$('.makeFullScreen').append(makeFS());
            var carousel = $("#carousel").featureCarousel({
                autoPlay:0,
                topPadding:-50,
                largeFeatureWidth:1,
                largeFeatureHeight:1,
                smallFeatureWidth:1,
                smallFeatureHeight:1,
                // include options like this:
                // (use quotes only for string values, and no trailing comma after last option)
                // option: value,
                // option: value
            });
        });
        

        
        /**
         * Loads data from the API.
         */
        function loadOldData() {
//            console.log('return');
            tilesCount = $("#tiles").children().size();
            isLoading = true;
            $('#loaderCircle').show();
            $.ajax({
                url: apiURL + '/all/' + NUMBER_CARDS + '/' + tilesCount, //offset
                success: onLoadOldData
            });
        };
        /**
         * Loads data from the API.
         */
        function loadNewData() {
            console.log('return');
            isLoading = true;
            $('#loaderCircle').show();
            $.ajax({
                url: apiURL + '/all/' + NUMBER_CARDS,
                success: onLoadNewData
            });
        };
        
        /**
         * Receives data from the API, creates HTML for images and updates the layout
         */
        function onLoadOldData(data) {
            isLoading = false;
            $('#loaderCircle').hide();

            // Increment page index for future calls.
            page++;
//            console.log(data);
            var obj = JSON.parse(data);
            for(var i = 0; i < obj.length; i++){
                $('#tiles').append(makeTile(obj[i]));
            }
            // Apply layout.
            applyLayout();
        };

        
        /**
         * Receives data from the API, creates HTML for images and updates the layout
         */
        function onLoadNewData(data) {
            isLoading = false;
            $('#loaderCircle').hide();

            /* Make new tiles based on the data given */
            var obj = JSON.parse(data);
            var newTiles = new Array();
            for (var i = 0; i < obj.length; i++) {
                if (eventTimeOfNewest == '') {
                    $('#tiles').append(makeTile(obj[i]));
                    $('#carousel').append(makeCarousel(obj[i]));
                } else if (new Date(eventTimeOfNewest).getTime() / 1000 < new Date(obj[i].event_datetime).getTime() / 1000) {
                    /* For the special edge case that a newly returned update has the same event_datetime
                     as the newest update. Use the id of the updates to distinguish instead. */
                    if (idOfNewest != obj[i].id) {
                        newTiles.push(makeTile(obj[i]));
                    }
                }
            }
            for (var i = newTiles.length - 1; i > -1 ; i--) {
                $('#tiles').prepend(newTiles[i]);
            }

            /* Track information about the newest update */
            idOfNewest = $('#tiles').find(">:first-child").find(">:first-child").data('id');
            eventTimeOfNewest = $('#tiles').find(">:first-child").find(">:first-child").data('event_datetime');

            /* Code to update all event datetime data */
            var tiles = $('#tiles');
            var eventDateTimes = Array();
            $('#tiles .contentStuff').each(function () {
                var content = $(this);
                eventDateTimes.push(content.data('event_datetime'));
            });
            $('#tiles .time_stamp_class').each(function (i) {
                var timeDiff = jQuery.timeago(eventDateTimes[i]+'+0000');
                $(this).html('<h8>'+timeDiff+'</h8>');
            });

            /* Apply layout. */
            applyLayout();
        };
        
        /**
         * Method to retrieve data from networks to be saved in the database, which will returned to the client later.
         * The method also updates the number of updates pending.
         */
        function retrieve(network){
            console.log('retrieve ' + network);
            $.ajax({
                url: "user/retrieve/"+network,
                success: displayPending
            });
        }

        /**
         * Retrieve with a return that follows immediately.
         */
        function retrieveReturn(network) {
            console.log('retrieve ' + network);
            $.ajax({
                url: "user/retrieve/"+network,
                success: displayPending
            }).done(function () {
                loadNewData();
            });
        }

        /**
         * Method to display information about pending updates ahead of next return.
         */
        function displayPending() {

        }

        $('#tiles').imagesLoaded(function() {
            // Get a reference to your grid items.
            handler = $('#tiles li');
            filters = $('#filters li');

            // Call the layout function.
            handler.wookmark(options);

            /**
             * When a filter is clicked, toggle it's active state and refresh.
             */
            var onClickFilter = function(event) {
                var item = $(event.currentTarget),
                    activeFilters = [];

                /* toggle active/inactive class state */
                if(item.hasClass('active')){
                    item.removeClass('active');
                    item.addClass('inactive');
                } else {
                    item.removeClass('inactive');
                    item.addClass('active');
                }

                /* Collect active filter strings */
                filters.filter('.active').each(function() {
                    if($(this).data('filter') != null){
                        activeFilters.push($(this).data('filter'));
                    }
                });
                
                /*Nhi's added for full screen*/
                curActiveFilter = activeFilters;
            
                
                handler.wookmarkInstance.filter(activeFilters, 'or');
            }

            /* Capture filter click events. */
            filters.click(onClickFilter);
        });

        /* Capture scroll event. */
        $(document).bind('scroll', onScroll);

        /* Load first data from the API. */
        loadNewData();
        retrieveReturn('');

        /* Set the retrieve and return timer */
        setInterval( function () { retrieve('facebook'); }, 45000 );
        setInterval( function () { retrieve('instagram'); }, 90000 );
        setInterval( function () { retrieve('youtube'); }, 120000 );
        setInterval( function () { retrieve('twitter'); }, 60000 );
        setInterval( function () { retrieve('tumblr'); }, 60000 );
        returnTimer = setInterval(loadNewData, 90000);

    })(jQuery);

    $(window).load(function(){
        $('#dvLoading').fadeOut(4000);
    });  
    
</script>


@stop
