@extends('master')

@section('content')

<div class='col-md-10 col-md-offset-1 well'>
    <h4>Welcome to your page, {{ ucwords(Auth::user()->username) }}.</h4>
    
    @if(!isset($home_timeline))
        <p>{{ HTML::link("oauth/authorize/twitter", "Authorize Twitter") }}</p>
    @endif
    <div id="main" role="main">
        @if(isset($home_timeline))
        <ul id="tiles">
            @foreach ($home_timeline as $update)
            <li>
                <img src="{{ $update->user->profile_image_url }}"><br>
                user-name: {{ $update->user->name }}<br>
                text: {{ $update->text}} <br>
                time: {{ $update->created_at }}<br>
                <span id="content1">9</span>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('#prev').click(function() {
            $.ajax({
                type: 'POST',
                url: 'ajax.php',
                data: 'id=testdata',
                dataType: 'json',
                cache: false,
                success: function(result) {
                  $('#content1').html(result[0]);
                }
            });
        });
    });
</script>

 <script type="text/javascript">
    (function ($){
      $('#tiles').imagesLoaded(function() {
        // Prepare layout options.
        var options = {
          autoResize: true, // This will auto-update the layout when the browser window is resized.
          container: $('#main'), // Optional, used for some extra CSS styling
          offset: 10, // Optional, the distance between grid items
          itemWidth: 210, // Optional, the width of a grid item
          fillEmptySpace: true // Optional, fill the bottom of each column with widths of flexible height
        };

        // Get a reference to your grid items.
        var handler = $('#tiles li');

        // Call the layout function.
        handler.wookmark(options);
      });
    })(jQuery);
  </script>
  
@stop