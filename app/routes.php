<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//ROUTES
Route::get('/', 'HomeController@getLogin');
Route::get('login', 'HomeController@getLogin');
Route::get('register', 'HomeController@getRegister');
Route::get('logout', 'HomeController@logout');
Route::get('user', array('before' => 'auth', 'uses' => 'UserController@displayHome'));

//POSTS
Route::post('register', 'HomeController@postRegister');
Route::post('login', 'HomeController@postLogin');

// Network Authorization
Route::get('authorize', array('before' => 'auth', 'uses' => 'NetAuthController@displayAuthorize'));

// Return
Route::get('user/return/{network?}/{limit?}/{offset?}', array('before' => 'auth', 'uses' => 'UpdatesController@returnUpdates'))
        ->where('limit', '[0-9]+')
        ->where('offset', '[0-9]+');

// Retrieve
Route::get('user/retrieve', array('before' => 'auth', 'uses' => 'UserController@massRetrieve'));
Route::get('user/retrieve/facebook/{limit?}', array('before' => 'auth', 'uses' => 'FacebookController@retrieveFacebook'));
Route::get('user/retrieve/instagram', array('before' => 'auth', 'uses' => 'InstagramController@retrieveInstagram'));
Route::get('user/retrieve/twitter', array('before' => 'auth', 'uses' => 'TwitterController@retrieveTwitter'));
Route::get('user/retrieve/youtube', array('before' => 'auth', 'uses' => 'YoutubeController@retrieveYoutube'));
Route::get('user/retrieve/tumblr', array('before' => 'auth', 'uses' => 'TumblrController@retrieveTumblr'));


// Delete Update
Route::get('user/delete/{id}', array('before' => 'auth', 'uses' => 'UpdatesController@deleteUpdate'));


// Facebook
Route::get('authorize/facebook', array('before' => 'auth', 'uses' => 'FacebookController@authorizeFacebook'));
Route::get('deauthorize/facebook', array('before' => 'auth', 'uses' => 'FacebookController@deauthorizeFacebook'));

// Instagram
Route::get('authorize/instagram', array('before' => 'auth', 'uses' => 'InstagramController@authorizeInstagram'));
Route::get('authorize/instagram/redirect', array('before' => 'auth', 'uses' => 'InstagramController@verifyInstagram'));
Route::get('deauthorize/instagram', array('before' => 'auth', 'uses' => 'InstagramController@deauthorizeInstagram'));

// Twitter
Route::get('authorize/twitter', array('before' => 'auth', 'uses' => 'TwitterController@authorizeTwitter'));
Route::get('authorize/twitter/redirect', array('before' => 'auth', 'uses' => 'TwitterController@verifyTwitter'));
Route::get('deauthorize/twitter', array('before' => 'auth', 'uses' => 'TwitterController@deauthorizeTwitter'));

// Youtube
Route::get('authorize/youtube', array('before' => 'auth', 'uses' => 'YoutubeController@authorizeYoutube'));
Route::get('authorize/youtube/redirect', array('before' => 'auth', 'uses' => 'YoutubeController@verifyYoutube'));
Route::get('deauthorize/youtube', array('before' => 'auth', 'uses' => 'YoutubeController@deauthorizeYoutube'));

// Tumblr
Route::get('authorize/tumblr', array('before' => 'auth', 'uses' => 'TumblrController@authorizeTumblr'));
Route::get('authorize/tumblr/redirect', array('before' => 'auth', 'uses' => 'TumblrController@verifyTumblr'));
Route::get('deauthorize/tumblr', array('before' => 'auth', 'uses' => 'TumblrController@deauthorizeTumblr'));

// RSS
Route::get('addrss/{link?}', 'RssController@addRss');
Route::get('removerss/{link?}', 'RssController@removeRss');
Route::get('getrss', 'RssController@getRss');
