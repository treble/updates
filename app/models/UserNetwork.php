<?php

class UserNetwork extends Eloquent {
    
    	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user_networks';
            
        public static function updateAccessTokens($uid, $nid, $token1, $token2 = NULL) {
            
            DB::table('user_networks')
                    ->where('user_id', $uid)
                    ->where('network_id', $nid)
                    ->update(array(
                        'access_token' => $token1, 
                        'access_token2' => $token2)
                            );
            
        }
        
        public static function updateLastRetrieve($uid, $nid, $time) {
            
            DB::table('user_networks')
                    ->where('user_id', $uid)
                    ->where('network_id', $nid)
                    ->update(array(
                        'last_retrieve' => $time)
                            );
            
        }
        
        public static function getRecord($uid, $nid) {
            
            return UserNetwork::whereRaw('user_id = ? and network_id = ?', array($uid, $nid))
                    ->limit(1)
                    ->get();
            
        }

        
        

        
}