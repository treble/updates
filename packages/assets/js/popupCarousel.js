/**
* Compile the Carousel for display
* @param jsonObj
* @returns {string}
*/

function makeCarousel(jsonObj){
    console.log('enter Carousel Function');
    var carouselString="";
    //var carouselBox=document.createElement("a");  
    //carouselBox.setAttribute("class", 'carousel-feature');
    carouselString = carouselString + '<div class="carousel-feature">';
    //carouselString = carouselString + '<a href="#"><img class=pp_"carousel-image" alt="Image Caption" src="packages/assets/icons/twitter-icon.png"></a>';
    carouselString = carouselString + '<div class="carousel-image"><div class=carouselItems>';
    if (jsonObj.network_name==='facebook'){
        carouselString = carouselString + '<div id=pp_cardFacebook>';
    }else if (jsonObj.network_name==='twitter'){
        carouselString = carouselString + '<div id=pp_cardTwitter>';
    }else if (jsonObj.network_name==='instagram'){
        carouselString = carouselString + '<div id=pp_cardInstagram>';
    }else if (jsonObj.network_name==='youtube'){
        carouselString = carouselString + '<div id=pp_cardYoutube>';
    }else if (jsonObj.network_name==='pinterest'){
        carouselString = carouselString + '<div id=pp_cardPinterest>';
    } 
    //END of card Set up

    //Adding display picture
    carouselString = carouselString + '<div id=pp_cardBanner><div id=pp_dp><img id=pp_dpEx src='+ jsonObj.profile_image +' width="60px" height="auto"></div>';
    
    //ADDING Display Name
    carouselString = carouselString + '<div id=pp_name><h13>'+jsonObj.name1+'</h13></div>';
    //ADDING Time Stamp
    var timeDiff = jQuery.timeago(jsonObj.event_datetime+'+0000');
    carouselString = carouselString + '<div id=pp_time_stamp class=pp_time_stamp_class><h8>'+timeDiff+'</h8></div>';
    //ADDING Network Icon
    if (jsonObj.network_name==='facebook'){
        carouselString = carouselString + '<div id=pp_networkIcon><img src="packages/assets/icons/facebook-icon.png" width="25px" height="25px"></div></div>';
    }else if (jsonObj.network_name==='twitter'){
        carouselString = carouselString + '<div id=pp_networkIcon><img src="packages/assets/icons/twitter-icon.png" width="25px" height="25px"></div></div>';
    }else if (jsonObj.network_name==='instagram'){
        carouselString = carouselString + '<div id=pp_networkIcon><img src="packages/assets/icons/instagram-icon.png" width="25px" height="25px"></div></div>';
    }else if (jsonObj.network_name==='youtube'){
        carouselString = carouselString + '<div id=pp_networkIcon><img src="packages/assets/icons/youtube-icon.png" width="25px" height="25px"></div></div>';
    }else if (jsonObj.network_name==='pinterest'){
        carouselString = carouselString + '<div id=pp_networkIcon><img src="packages/assets/icons/pinterest-icon.png" width="25px" height="25px"></div></div>';
    } 
    //ADDING content title
    carouselString = carouselString + '<div id=pp_dataTypeString><h9>'+jsonObj.title1+'<h9></div>';        
    
    //ADDING Media   
    var media_fullScreen;
    if (jsonObj.media_large_1 !== null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null){
        carouselString = carouselString + '<div id=pp_media1>';
        carouselString = carouselString + '<div id=pp_mediaImg1><img src="'+jsonObj.media_large_1+'" width="100%" height="auto"></div>';
        media_fullScreen = jsonObj.media_large_1;
        carouselString = carouselString + '</div><br>';//closing off media1 2 3 4
    }else if (jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        carouselString = carouselString + '<div id=pp_media1>';
        carouselString = carouselString + '<div id=pp_mediaImg1><img src="'+jsonObj.media_small_1+'" width="100%" height="auto"></div>';
        media_fullScreen = jsonObj.media_small_1;
        carouselString = carouselString + '</div><br>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null &&  jsonObj.update_type==='video'){
        carouselString = carouselString + '<div id=pp_media1>';
        carouselString = carouselString + '<div id=pp_mediaVid1><video src="'+jsonObj.media_large_1+'" controls width="180px" height="auto"></video></div>';        
        media_fullScreen = jsonObj.media_large_1;
        carouselString = carouselString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 !== null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 === null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        carouselString = carouselString + '<div id=pp_media2>';
        carouselString = carouselString + '<div id=pp_mediaImg2_1><img src="'+jsonObj.media_large_1+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg2_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';       
        carouselString = carouselString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 !== null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        carouselString = carouselString + '<div id=pp_media2>';
        carouselString = carouselString + '<div id=pp_mediaImg2_1><img src="'+jsonObj.media_small_1+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg2_2><img src="'+jsonObj.media_small_2+'" width="87.5px" height="auto"></div>';       
        carouselString = carouselString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 !== null && jsonObj.media_large_3 !== null && jsonObj.media_large_4 === null && jsonObj.media_small_1 === null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        carouselString = carouselString + '<div id=pp_media3>';
        carouselString = carouselString + '<div id=pp_mediaImg3_1><img src="'+jsonObj.media_large_1+'" width="180px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg3_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg3_3><img src="'+jsonObj.media_large_3+'" width="87.5px" height="auto" align="middle"></div>';      
        carouselString = carouselString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 !== null && jsonObj.media_small_3 !== null && jsonObj.media_small_4 === null){
        carouselString = carouselString + '<div id=pp_media3>';
        carouselString = carouselString + '<div id=pp_mediaImg3_1><img src="'+jsonObj.media_small_1+'" width="180px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg3_2><img src="'+jsonObj.media_small_2+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg3_3><img src="'+jsonObj.media_small_3+'" width="87.5px" height="auto" align="middle"></div>';      
        carouselString = carouselString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 !== null && jsonObj.media_large_3 !== null && jsonObj.media_large_4 !== null && jsonObj.media_small_1 === null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        carouselString = carouselString + '<div id=pp_media4>';
        carouselString = carouselString + '<div id=pp_mediaImg4_1><img src="'+jsonObj.media_large_1+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg4_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg4_3><img src="'+jsonObj.media_large_3+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg4_4><img src="'+jsonObj.media_large_4+'" width="87.5px" height="auto"></div>';     
        carouselString = carouselString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 !== null && jsonObj.media_small_3 !== null && jsonObj.media_small_4 !== null){
        carouselString = carouselString + '<div id=pp_media4>';
        carouselString = carouselString + '<div id=pp_mediaImg4_1><img src="'+jsonObj.media_large_1+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg4_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg4_3><img src="'+jsonObj.media_large_3+'" width="87.5px" height="auto"></div>';
        carouselString = carouselString + '<div id=pp_mediaImg4_4><img src="'+jsonObj.media_large_4+'" width="87.5px" height="auto"></div>';     
        carouselString = carouselString + '</div>';//closing off media1 2 3 4
    }
    
    
    //ADDING STATISTICS
    if (jsonObj.network_name==='facebook'){
        carouselString = carouselString + '<div id=pp_fbStat>';
        carouselString = carouselString + '<div id=pp_fbLikeIcon><img src="packages/assets/images/fbLike-icon.png" width="15px" height="15px"></div>';
        carouselString = carouselString + '<div id=pp_fbLike><h12>' + jsonObj.statistic1 + '</h12></div>';
        carouselString = carouselString + '<div id=pp_fbCommentIcon><img src="packages/assets/images/fbComment-icon.png" width="15px" height="15px"></div>';
        carouselString = carouselString + '<div id=pp_fbComment><h12>' + jsonObj.statistic2 + '</h12></div>';
        carouselString = carouselString + '</div>';
    }else if (jsonObj.network_name==='twitter'){
        carouselString = carouselString + '<div id=pp_twitterStat><div id=pp_retweets><h12>RETWEETS ' + jsonObj.statistic1 + '</h12></div>';
        carouselString = carouselString + '<div id=pp_favourites><h12>FAVOURITES ' + jsonObj.statistic2 + '</h12></div>';
        carouselString = carouselString + '</div>';
    }else if (jsonObj.network_name==='instagram'){
        carouselString = carouselString + '<div id=pp_InStat>';
        carouselString = carouselString + '<div id=pp_InLikeIcon><img src="packages/assets/images/InLike-icon.png" width="14px" height="14px"></div>';
        carouselString = carouselString + '<div id=pp_InLike><h12>' + jsonObj.statistic1 + '</h12></div>';
        carouselString = carouselString + '<div id=pp_InCommentIcon><img src="packages/assets/images/InComment-icon.png" width="14px" height="14px"></div>';
        carouselString = carouselString + '<div id=pp_InComment><h12>' + jsonObj.statistic2 + '</h12></div>';
        carouselString = carouselString + '</div>';
    }else if (jsonObj.network_name==='youtube'){
        carouselString = carouselString + '<div id=pp_ytStat1>';
        carouselString = carouselString + '<div id=pp_ytLikeIcon><img src="packages/assets/images/ytLike-icon.png" width="14px" height="14px"></div>';
        carouselString = carouselString + '<div id=pp_ytLike><h12>' + jsonObj.statistic1 + '</h12></div>';
        carouselString = carouselString + '<div id=pp_ytUnlikeIcon><img src="packages/assets/images/ytUnlike-icon.png" width="14px" height="14px"></div>';
        carouselString = carouselString + '<div id=pp_ytUnlike><h12>' + jsonObj.statistic2 + '</h12></div>';
        carouselString = carouselString + '</div>';
        carouselString = carouselString + '<div id=pp_ytStat2>';
        carouselString = carouselString + '<div id=pp_ytCommentIcon><img src="packages/assets/images/ytComment-icon.png" width="14px" height="14px"></div>';
        carouselString = carouselString + '<div id=pp_ytLike><h12>' + jsonObj.statistic3 + '</h12></div>';
        carouselString = carouselString + '<div id=pp_ytWatchIcon><img src="packages/assets/images/ytWatch-icon.png" width="14px" height="14px"></div>';
        carouselString = carouselString + '<div id=pp_ytUnlike><h12>' + jsonObj.statistic4 + '</h12></div>';
        carouselString = carouselString + '</div>';
    }else if (jsonObj.network_name==='pinterest'){
        //carouselString = carouselString + '<div id=pp_networkIcon><img src="packages/assets/icons/pinterest-icon.png" width="25px" height="25px"></div></div>';
    }  
    
    //ADDING text
    if (jsonObj.text1 !== null && jsonObj.text2===null && jsonObj.text3===null && jsonObj.text4===null){
        carouselString = carouselString + '<div id=pp_text1><h11>'+jsonObj.text1+'</h11></div>';
        carouselString = carouselString + '<div id=pp_spacer1></div>';
    }
    if (jsonObj.text1 !== null && jsonObj.text2!==null && jsonObj.text3===null && jsonObj.text4===null){
        carouselString = carouselString + '<div id=pp_text1><h11>'+jsonObj.text1+'</h11></div>';
        carouselString = carouselString + '<div id=pp_text2><h11>'+jsonObj.text2+'</h11></div>';
        carouselString = carouselString + '<div id=pp_spacer2></div>';
    }
    if (jsonObj.text1 !== null && jsonObj.text2!==null && jsonObj.text3!==null && jsonObj.text4===null){
        carouselString = carouselString + '<div id=pp_text1><h11>'+jsonObj.text1+'</h11></div>';
        carouselString = carouselString + '<div id=pp_text2><h11>'+jsonObj.text2+'</h11></div>';       
        carouselString = carouselString + '<div id=pp_text3><h11>'+jsonObj.text3+'</h11></div>';
        carouselString = carouselString + '<div id=pp_spacer3></div>';
    }
    if (jsonObj.text1 !== null && jsonObj.text2!==null && jsonObj.text3!==null && jsonObj.text4!==null){
        carouselString = carouselString + '<div id=pp_text1><h11>'+jsonObj.text1+'</h11></div>';
        carouselString = carouselString + '<div id=pp_text2><h11>'+jsonObj.text2+'</h11></div>';       
        carouselString = carouselString + '<div id=pp_text3><h11>'+jsonObj.text3+'</h11></div>';
        carouselString = carouselString + '<div id=pp_text4><h11>'+jsonObj.text4+'</h11></div>';
        carouselString = carouselString + '<div id=pp_spacer4></div>';
    }  
    //Add Spacer
    carouselString = carouselString + '<br>';
    //carouselString = carouselString + '<div id=pp_spacerline></div>';
    
    //ADDING source bookmark fullscreen close icons
    carouselString = carouselString + '</div>';//End contentItems
    
    
    //carouselString = carouselString + '<img alt="Image Caption" src="packages/assets/icons/twitter-icon.png" height = 100% width = 100%>';
    carouselString = carouselString + '</div>';//end div of carouselItems
    carouselString = carouselString + '</div>';//end div of carouselImages
    carouselString = carouselString + '</div>';//end div of carousel-features
    //carouselBox.innerHTML = carouselString;
    return carouselString;
} 
