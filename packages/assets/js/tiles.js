/**
* Compile the tile for display
* @param jsonObj
* @returns {string}
*/
function makeTile(jsonObj){
    //console.log(jsonObj);
    //START of card Set up
    var contentDataString="";
    var contentBox=document.createElement("div");
    contentBox.setAttribute("id", 'contentBox_'+ jsonObj.id);
    var contentBoxListItem = document.createElement("li");
    contentBoxListItem.setAttribute("id", 'contentItems_'+ jsonObj.id);
    contentBoxListItem.setAttribute("class", 'contentStuff');
    contentBoxListItem.setAttribute("data-event_datetime", jsonObj.event_datetime);
    contentBoxListItem.setAttribute("data-filter-class", '["'+jsonObj.network_name+'"]');
    if (jsonObj.network_name==='facebook'){
        contentDataString = contentDataString + '<div id=cardFacebook>';
    }else if (jsonObj.network_name==='twitter'){
        contentDataString = contentDataString + '<div id=cardTwitter>';
    }else if (jsonObj.network_name==='instagram'){
        contentDataString = contentDataString + '<div id=cardInstagram>';
    }else if (jsonObj.network_name==='youtube'){
        contentDataString = contentDataString + '<div id=cardYoutube>';
    }else if (jsonObj.network_name==='pinterest'){
        contentDataString = contentDataString + '<div id=cardPinterest>';
    }else if (jsonObj.network_name==='tumblr'){
        contentDataString = contentDataString + '<div id=cardTumblr>';
    }else{
        contentDataString = contentDataString + '<div id=cardDefault>';
    }
    //END of card Set up

    //Adding display picture
    contentDataString = contentDataString + '<div id=cardBanner><div id=dp><img id=dpEx src='+ jsonObj.profile_image +' width="72px" height="auto"></div>';
    
    //ADDING Display Name
    contentDataString = contentDataString + '<div id=name><h13>'+jsonObj.name1+'</h13></div>';
    //ADDING Time Stamp
    var timeDiff = jQuery.timeago(jsonObj.event_datetime+'+0000');
    contentDataString = contentDataString + '<div id=time_stamp class=time_stamp_class><h8>'+timeDiff+'</h8></div>';
    //ADDING Network Icon
    if (jsonObj.network_name==='facebook'){
        contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/facebook-icon.png" width="30px" height="30px"></div></div>';
    }else if (jsonObj.network_name==='twitter'){
        contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/twitter-icon.png" width="30px" height="30px"></div></div>';
    }else if (jsonObj.network_name==='instagram'){
        contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/instagram-icon.png" width="30px" height="30px"></div></div>';
    }else if (jsonObj.network_name==='youtube'){
        contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/youtube-icon.png" width="30px" height="30px"></div></div>';
    }else if (jsonObj.network_name==='pinterest'){
        contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/pinterest-icon.png" width="30px" height="30px"></div></div>';
    }else if (jsonObj.network_name==='tumblr'){
        contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/tumblr-icon.png" width="30px" height="30px"></div></div>';
    }else{
        contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/default-icon.png" width="30px" height="30px"></div></div>';
    } 
    //ADDING content title
    contentDataString = contentDataString + '<div id=dataTypeString><h9>'+jsonObj.title1+'<h9></div>';        
    
    //ADDING Media   
    var media_fullScreen;
    if (jsonObj.media_large_1 !== null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null){
        contentDataString = contentDataString + '<div id=media1>';
        contentDataString = contentDataString + '<div id=mediaImg1><img src="'+jsonObj.media_large_1+'" width="100%" height="auto"></div>';
        media_fullScreen = jsonObj.media_large_1;
        contentDataString = contentDataString + '</div><br>';//closing off media1 2 3 4
    }else if (jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        contentDataString = contentDataString + '<div id=media1>';
        contentDataString = contentDataString + '<div id=mediaImg1><img src="'+jsonObj.media_small_1+'" width="100%" height="auto"></div>';
        media_fullScreen = jsonObj.media_small_1;
        contentDataString = contentDataString + '</div><br>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null &&  jsonObj.update_type==='video'){
        contentDataString = contentDataString + '<div id=media1>';
        contentDataString = contentDataString + '<div id=mediaVid1><video src="'+jsonObj.media_large_1+'" controls width="180px" height="auto"></video></div>';        
        media_fullScreen = jsonObj.media_large_1;
        contentDataString = contentDataString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 !== null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 === null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        contentDataString = contentDataString + '<div id=media2>';
        contentDataString = contentDataString + '<div id=mediaImg2_1><img src="'+jsonObj.media_large_1+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg2_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';       
        contentDataString = contentDataString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 !== null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        contentDataString = contentDataString + '<div id=media2>';
        contentDataString = contentDataString + '<div id=mediaImg2_1><img src="'+jsonObj.media_small_1+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg2_2><img src="'+jsonObj.media_small_2+'" width="87.5px" height="auto"></div>';       
        contentDataString = contentDataString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 !== null && jsonObj.media_large_3 !== null && jsonObj.media_large_4 === null && jsonObj.media_small_1 === null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        contentDataString = contentDataString + '<div id=media3>';
        contentDataString = contentDataString + '<div id=mediaImg3_1><img src="'+jsonObj.media_large_1+'" width="180px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg3_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg3_3><img src="'+jsonObj.media_large_3+'" width="87.5px" height="auto" align="middle"></div>';      
        contentDataString = contentDataString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 !== null && jsonObj.media_small_3 !== null && jsonObj.media_small_4 === null){
        contentDataString = contentDataString + '<div id=media3>';
        contentDataString = contentDataString + '<div id=mediaImg3_1><img src="'+jsonObj.media_small_1+'" width="180px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg3_2><img src="'+jsonObj.media_small_2+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg3_3><img src="'+jsonObj.media_small_3+'" width="87.5px" height="auto" align="middle"></div>';      
        contentDataString = contentDataString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 !== null && jsonObj.media_large_2 !== null && jsonObj.media_large_3 !== null && jsonObj.media_large_4 !== null && jsonObj.media_small_1 === null && jsonObj.media_small_2 === null && jsonObj.media_small_3 === null && jsonObj.media_small_4 === null){
        contentDataString = contentDataString + '<div id=media4>';
        contentDataString = contentDataString + '<div id=mediaImg4_1><img src="'+jsonObj.media_large_1+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg4_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg4_3><img src="'+jsonObj.media_large_3+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg4_4><img src="'+jsonObj.media_large_4+'" width="87.5px" height="auto"></div>';     
        contentDataString = contentDataString + '</div>';//closing off media1 2 3 4
    }else if(jsonObj.media_large_1 === null && jsonObj.media_large_2 === null && jsonObj.media_large_3 === null && jsonObj.media_large_4 === null && jsonObj.media_small_1 !== null && jsonObj.media_small_2 !== null && jsonObj.media_small_3 !== null && jsonObj.media_small_4 !== null){
        contentDataString = contentDataString + '<div id=media4>';
        contentDataString = contentDataString + '<div id=mediaImg4_1><img src="'+jsonObj.media_large_1+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg4_2><img src="'+jsonObj.media_large_2+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg4_3><img src="'+jsonObj.media_large_3+'" width="87.5px" height="auto"></div>';
        contentDataString = contentDataString + '<div id=mediaImg4_4><img src="'+jsonObj.media_large_4+'" width="87.5px" height="auto"></div>';     
        contentDataString = contentDataString + '</div>';//closing off media1 2 3 4
    }
    
    
    //ADDING STATISTICS
    if (jsonObj.network_name==='facebook'){
        contentDataString = contentDataString + '<div id=fbStat>';
        contentDataString = contentDataString + '<div id=fbLikeIcon><img src="packages/assets/images/fbLike-icon.png" width="13px" height="13px"></div>';
        contentDataString = contentDataString + '<div id=fbLike><h12>' + jsonObj.statistic1 + '</h12></div>';
        contentDataString = contentDataString + '<div id=fbCommentIcon><img src="packages/assets/images/fbComment-icon.png" width="12px" height="12px"></div>';
        contentDataString = contentDataString + '<div id=fbComment><h12>' + jsonObj.statistic2 + '</h12></div>';
        contentDataString = contentDataString + '</div>';
    }else if (jsonObj.network_name==='twitter'){
        contentDataString = contentDataString + '<div id=twitterStat><div id=retweets><h12>RETWEETS ' + jsonObj.statistic1 + '</h12></div>';
        contentDataString = contentDataString + '<div id=favourites><h12>FAVOURITES ' + jsonObj.statistic2 + '</h12></div>';
        contentDataString = contentDataString + '</div>';
    }else if (jsonObj.network_name==='instagram'){
        contentDataString = contentDataString + '<div id=InStat>';
        contentDataString = contentDataString + '<div id=InLikeIcon><img src="packages/assets/images/InLike-icon.png" width="14px" height="14px"></div>';
        contentDataString = contentDataString + '<div id=InLike><h12>' + jsonObj.statistic1 + '</h12></div>';
        contentDataString = contentDataString + '<div id=InCommentIcon><img src="packages/assets/images/InComment-icon.png" width="14px" height="14px"></div>';
        contentDataString = contentDataString + '<div id=InComment><h12>' + jsonObj.statistic2 + '</h12></div>';
        contentDataString = contentDataString + '</div>';
    }else if (jsonObj.network_name==='youtube'){
        contentDataString = contentDataString + '<div id=ytStat1>';
        contentDataString = contentDataString + '<div id=ytLikeIcon><img src="packages/assets/images/ytLike-icon.png" width="14px" height="14px"></div>';
        contentDataString = contentDataString + '<div id=ytLike><h12>' + jsonObj.statistic1 + '</h12></div>';
        contentDataString = contentDataString + '<div id=ytUnlikeIcon><img src="packages/assets/images/ytUnlike-icon.png" width="14px" height="14px"></div>';
        contentDataString = contentDataString + '<div id=ytUnlike><h12>' + jsonObj.statistic2 + '</h12></div>';
        contentDataString = contentDataString + '</div>';
        contentDataString = contentDataString + '<div id=ytStat2>';
        contentDataString = contentDataString + '<div id=ytCommentIcon><img src="packages/assets/images/ytComment-icon.png" width="14px" height="14px"></div>';
        contentDataString = contentDataString + '<div id=ytLike><h12>' + jsonObj.statistic3 + '</h12></div>';
        contentDataString = contentDataString + '<div id=ytWatchIcon><img src="packages/assets/images/ytWatch-icon.png" width="14px" height="14px"></div>';
        contentDataString = contentDataString + '<div id=ytUnlike><h12>' + jsonObj.statistic4 + '</h12></div>';
        contentDataString = contentDataString + '</div>';
    }else if (jsonObj.network_name==='pinterest'){
        //contentDataString = contentDataString + '<div id=networkIcon><img src="packages/assets/icons/pinterest-icon.png" width="25px" height="25px"></div></div>';
    }else if (jsonObj.network_name==='tumblr'){
        contentDataString = contentDataString + '<div id=tumblrStat><div id=tumblrNotes><h12>' + jsonObj.statistic1 + ' Notes</h12></div>';
        contentDataString = contentDataString + '</div>';
    }
    
    //ADDING text
    if (jsonObj.text1 !== null && jsonObj.text2===null && jsonObj.text3===null && jsonObj.text4===null){
        contentDataString = contentDataString + '<div id=text1><h11>'+jsonObj.text1+'</h11></div>';
        contentDataString = contentDataString + '<div id=spacer1></div>';
    }
    if (jsonObj.text1 !== null && jsonObj.text2!==null && jsonObj.text3===null && jsonObj.text4===null){
        contentDataString = contentDataString + '<div id=text1><h11>'+jsonObj.text1+'</h11></div>';
        contentDataString = contentDataString + '<div id=text2><h11>'+jsonObj.text2+'</h11></div>';
        contentDataString = contentDataString + '<div id=spacer2></div>';
    }
    if (jsonObj.text1 !== null && jsonObj.text2!==null && jsonObj.text3!==null && jsonObj.text4===null){
        contentDataString = contentDataString + '<div id=text1><h11>'+jsonObj.text1+'</h11></div>';
        contentDataString = contentDataString + '<div id=text2><h11>'+jsonObj.text2+'</h11></div>';       
        contentDataString = contentDataString + '<div id=text3><h11>'+jsonObj.text3+'</h11></div>';
        contentDataString = contentDataString + '<div id=spacer3></div>';
    }
    if (jsonObj.text1 !== null && jsonObj.text2!==null && jsonObj.text3!==null && jsonObj.text4!==null){
        contentDataString = contentDataString + '<div id=text1><h11>'+jsonObj.text1+'</h11></div>';
        contentDataString = contentDataString + '<div id=text2><h11>'+jsonObj.text2+'</h11></div>';       
        contentDataString = contentDataString + '<div id=text3><h11>'+jsonObj.text3+'</h11></div>';
        contentDataString = contentDataString + '<div id=text4><h11>'+jsonObj.text4+'</h11></div>';
        contentDataString = contentDataString + '<div id=spacer4></div>';
    }  
    //Add Spacer
    contentDataString = contentDataString + '<br>';
    //contentDataString = contentDataString + '<div id=spacerline></div>';
    
    //ADDING source bookmark fullscreen close icons
    contentDataString = contentDataString + '</div>';//End contentItems
    contentDataString = contentDataString + '<div id="bottom-controls">';//End contentItems
    
    contentDataString = contentDataString + '<div id=sourceIcon><a href="'+ jsonObj.source_url +'" class="bottomControls" title="View Source" target="_blank"><img src="packages/assets/images/source-icon.png" width="12px" height="12px"></a></div>';
    contentDataString = contentDataString + '<div id=bookmarkIcon><a href="#" class="bottomControls" title="Bookmark"><img src="packages/assets/images/bookmark-icon.png" width="12px" height="12px"></a></div>';
    contentDataString = contentDataString + '<div id=fullScreenIcon><a bottomControls" title="Full Screen" href="#" onClick="return false;"><img src="packages/assets/images/fullScreen-icon.png" width="12px" height="12px"></a></div>';
    contentDataString = contentDataString + '<div id=closeIcon><a href="#" class="bottomControls" title="Close" onClick="return false;"><img src="packages/assets/images/close-icon.png" width="12px" height="12px"></a></div>';
    contentDataString = contentDataString + '</div>';//End bottomControls
    
    //Adding Tooltip
    var tooltipOptions = {
        animation: 'true',
        placement: 'top',
        trigger: 'hover'
    };
    $('.bottomControls').tooltip(tooltipOptions);
    
    
    //adding Fullscreen functionality
    contentDataString = contentDataString + '<div id="fullScreen_'+jsonObj.id+'" class="popUp">';
    contentDataString = contentDataString + '<h1>ID='+jsonObj.id+'</h1>';                
//    contentDataString = contentDataString + '<div id=fullScreenMedia><img src="'+media_fullScreen+'" width="auto" height="auto"></div>';
//    contentDataString = contentDataString + '<br><br><button class="fadeandscale_'+jsonObj.id+'_close btn btn-default">Close</button>';
    contentDataString = contentDataString + '<div>';
//    $('#fadeandscale_'+jsonObj.id).popup();


    contentBoxListItem.innerHTML = contentDataString;
    contentBox.appendChild(contentBoxListItem);
    return contentBox;
} 


function makeCarousel(jsonObj){
    alert('enter Carousel Function' + jsonObj);
    var carouselString="";
    //var carouselBox=document.createElement("a");  
    //carouselBox.setAttribute("class", 'carousel-feature');
    
    carouselString = carouselString + '<a href="#"><img class="carousel-image" alt="Image Caption" src="packages/assets/icons/twitter-icon.png"></a>';
    
    //carouselBox.innerHTML = carouselString;
    return carouselString;
} 

